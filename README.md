<!--
This file is part of the replication artifact for
difference verification with conditions:
https://gitlab.com/sosy-lab/research/data/difference-data

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Artifact for Submission "Difference Verification with Conditions", SEFM'20

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/sosy-lab%2Fresearch%2Fdata%2Fdifference-data/SEFM20-proceedings?filepath=Analysis.ipynb)

## Analyzing data/creating tables and figures

### Requirements

We provide a docker image with this repository:
`registry.gitlab.com/sosy-lab/research/data/difference-data/jupyter`


For use without docker, the artifact has the following requirements:

- python >= 3.6.9
- texlive >= 2017

Python packages required:
- matplotlib >= 3.2
- benchexec >= 2.6
- pandas >= 1.03
- jupyter notebook >= 6.0.3

These can be installed, for example, with `pip -r requirements.txt`.

### Analysis

Data analysis and content is created through Jupyter Notebook `Analysis.ipynb`.
The Notebook will automatically use the existing data from folder `data-submission`, unless you created own data in folder `data_raw` (see below).

To start the Jupyter Notebook through Docker, run from this README's directory:

```bash
docker run -p 8888:8888 -it --rm --volume $(pwd):/data registry.gitlab.com/sosy-lab/research/data/difference-data/jupyter
```

If you want to run the notebook without Docker, run `jupyter notebook` from this artifact's folder.

The server output will tell you how to access the running notebook; usually through [`http://localhost:8888/tree?token=sosy`][2].

[2]: http://localhost:8888/tree?token=sosy


## Benchmark tasks and initial Predicates

The used benchmark tasks are in folder `svcomp_combination_tasks/`.
The predicates that were used for precision reuse are in folder `precisions/`.


## Running experiments

Since running experiments takes a vast amount of time (upper bound: ~5 CPU years),
we don't expect this to be the primary use-case of this artifact.
Instead, our own produced data can be found in folder `data-submission` and will be automatically used
by the data analysis as long as no folder `data_raw` exists.

All experiments are run through jobs defined in `Makefile`.

### Requirements

If you want to reproduce the experiments, you require:

- python >= 3.6.9
- python 3.6
- GNU make >= 4.1
- Java >= 11
- Java 8
- ant >= 1.10
- wget >= 1.20
- a [proper installation][1] of benchexec >= 2.6.

[1]: https://github.com/sosy-lab/benchexec/blob/master/doc/INSTALL.md

Running experiments requires at least 5 (virtual) CPU cores and 17 GB of memory.
If you want to run experiments in a virtual machine, make sure to adjust the settings accordingly.

### Setup for running experiments

To initialize the repository for running experiments, run `git submodule update --init` .

### Re-generating benchmark tasks

Run `make tasks` to re-generate the combo tasks.

### Executing benchmarks

Run `make all` to run all experiments. This may also regenerate the combo tasks if Make thinks this is necessary.



## Tutorial on Difference Verification with Conditions

### Installation of Tools

Run the following command from the root directory
of this project to download the necessary tools (CPAchecker, CPA-seq and UAutomizer):

```shell
make tools
```

For this tutorial, we use the example [absSum.c](examples/absSum.c)
and its modified version [absSumMod.c](examples/absSumMod.c), as listed in the paper.

Next, we explain how to perform difference verification with the three verifiers used in the paper.
Regardless of the verifier, difference verification with conditions consists of two steps:
(1) algorithm diffCond and (2) conditional verification.


### Difference Verification with Predicate Analysis

Since the diffCond algorithm and the predicate analysis are both implemented in CPAchecker,
we realize difference verification with predicate analysis as a sequential analysis of the two components
"diffCond algorithm" and "predicate analysis".
To run difference verification with conditions on our tutorial example,
execute the command below from within folder `cpachecker`.


```shell
scripts/cpa.sh -differentialPredicateAnalysis -setprop differential.program=../examples/absSum.c \
  ../examples/absSumMod.c \
  -spec ../examples/unreach-call.prp
```

You should see the following output (see [below](#expected-outputs) for full output):

```text
Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 26) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
Graphical representation included in the file "./output/Counterexample.2.html".
```

To find out why predicate analysis deems the program to reach the error-location, you can take a look at the files `output/Counterexample.2.*`.

### Difference Verification with CPA-Seq and UAutomizer

Both verifiers CPA-seq and UAutomizer are no conditional verifiers.
Thus, we use the [reducer-based construction][3] to turn them into conditional verifiers.
In our case, the reducer gets the condition produced by algorithm diffCond and generates a residual program.
Thereafter, the verifiers analyze the residual program.
Since algorithm diffCond and the reducer are both implemented in CPAchecker,
we run them in sequence.
In a second step, the respective verifiers can be applied to the residual program.

1. To generate the residual program you need to execute the command below.
    Our description assumes that you execute the command from within folder `cpachecker`.

      ```shell
      scripts/cpa.sh -differentialProgramGenerator -setprop differential.program=../examples/absSum.c \
        ../examples/absSumMod.c \
        -setprop residualprogram.strategy=CONDITION_PLUS_FOLD \
        -setprop residualprogram.folderType=CFA \
        -setprop differential.ignoreDeclarations=true \
        -spec ../examples/unreach-call.prp
      ```

    You should see the following output (see [below](#expected-outputs) for full output):

      ```text
      Finished.
      More details about the verification run can be found in the directory "./output".
      ```

    The residual program is written to `output/residual_program.c`.

2. Execution of the verifier
      - To execute CPA-Seq on the residual program, use the command below.
        We assume that you execute the command from within folder `cpa-seq`
        and that the residual program was created at `cpachecker/output/residual_program.c`.

          ```shell
          scripts/cpa.sh -svcomp20 ../cpachecker/output/residual_program.c \
            -spec ../examples/unreach-call.prp \
            -timelimit 900
          ```
        You should see the following output (see [below](#expected-outputs) for full output):

          ```text
          Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 19) found by chosen configuration.
          More details about the verification run can be found in the directory "./output".
          Graphical representation included in the file "./output/Counterexample.1.html".
          ```
        To find out why CPA-seq deems the program to reach the error-location, you can take a look at the files `output/Counterexample.1.*`.

      - To execute UAutomizer on the residual program, use the following command.
        We assume that you execute the command from within folder `uautomizer`
        and that the residual program was created at `cpachecker/output/residual_program.c`.

          ```shell
          ./Ultimate.py --file ../cpachecker/output/residual_program.c \
            --spec ../examples/unreach-call.prp --architecture 32bit
          ```

        You should see the following output (see [below](#expected-outputs) for full output):

          ```text
          Execution finished normally
          Writing output log to file Ultimate.log
          Result:
          TRUE
          ```

          UAutomizer did not find any error.

[3]: https://dl.acm.org/doi/abs/10.1145/3180155.3180259

### Expected Outputs

For each command, we list the expected output here.

Command:

```shell
scripts/cpa.sh -differentialPredicateAnalysis -setprop differential.program=../examples/absSum.c \
  ../examples/absSumMod.c \
  -spec ../examples/unreach-call.prp
```

Output:

```
Running CPAchecker with default heap size (1200M). Specify a larger value with -heap if you have more RAM.
Running CPAchecker with default stack size (1024k). Specify a larger value with -stack if needed.
Language C detected and set for analysis (CPAMain.detectFrontendLanguageIfNecessary, INFO)

CPAchecker 1.9.1-svn-230d2ca59d / differentialPredicateAnalysis (OpenJDK 64-Bit Server VM 11.0.7) started (CPAchecker.run, INFO)

Parsing CFA from file(s) "../examples/absSumMod.c" (CPAchecker.parse, INFO)

Using Restarting Algorithm (CoreComponentsFactory.createAlgorithm, INFO)

The following configuration options were specified but are not used:
 differential.program 
 (CPAchecker.printConfigurationWarnings, WARNING)

Starting analysis ... (CPAchecker.runAlgorithm, INFO)

Loading analysis 1 from file config/components/differentialAutomatonGeneratorAsComponent.properties ... (RestartAlgorithm.run, INFO)

Mismatch of configuration options when loading from 'config/components/differentialAutomatonGeneratorAsComponent.properties': 'specification' has two values 'config/specification/sv-comp-reachability.spc' and '../specification/default.spc'. Using '../specification/default.spc'. (NestingAlgorithm.checkConfigs, INFO)

Starting analysis 1 ... (RestartAlgorithm.run, INFO)

Statistics for algorithm 1 of 2
===============================
Total time for algorithm 1:     0.979s

AutomatonAnalysis (ModificationsAutomaton) statistics
-----------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.002s
Total time for strengthen operator:                    0.104s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 23, count: 23, min: 1, max: 1) [1 x 23]
Number of states with assumption transitions:      0

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.001s
Automaton transfers with branching:                0
Automaton transfer successors:                     0.96 (sum: 22, count: 23, min: 0, max: 1) [0, 1 x 22]
Number of states with assumption transitions:      0

CPA algorithm statistics
------------------------
Number of iterations:            22
Max size of waitlist:            3
Average size of waitlist:        1
Number of computed successors:   22
Max successors for one state:    2
Number of times merged:          0
Number of times stopped:         1
Number of times breaked:         0

Total time for CPA algorithm:         0.194s (Max:     0.194s)
  Time for choose from waitlist:      0.000s
  Time for precision adjustment:      0.052s
  Time for transfer relation:         0.139s
  Time for stop operator:             0.001s
  Time for adding to reached set:     0.001s

Assumption Collection algorithm statistics
------------------------------------------
Number of locations with assumptions:              1
Number of states in automaton:                     10

CPA algorithm statistics
------------------------
Number of iterations:            22
Max size of waitlist:            3
Average size of waitlist:        1
Number of computed successors:   22
Max successors for one state:    2
Number of times merged:          0
Number of times stopped:         1
Number of times breaked:         0

Total time for CPA algorithm:         0.194s (Max:     0.194s)
  Time for choose from waitlist:      0.000s
  Time for precision adjustment:      0.052s
  Time for transfer relation:         0.139s
  Time for stop operator:             0.001s
  Time for adding to reached set:     0.001s

Assumption Collection algorithm statistics
------------------------------------------
Number of locations with assumptions:              1
Number of states in automaton:                     20

RestartAlgorithm switches to the next configuration... (RestartAlgorithm.run, INFO)

Loading analysis 2 from file config/components/predicateAnalysis-use-cmc-condition-differential.properties ... (RestartAlgorithm.run, INFO)

Mismatch of configuration options when loading from 'config/components/predicateAnalysis-use-cmc-condition-differential.properties': 'specification' has two values 'config/specification/sv-comp-reachability.spc' and '../specification/default.spc'. Using '../specification/default.spc'. (NestingAlgorithm.checkConfigs, INFO)

Using the following resource limits: CPU-time limit of 900s (Analysis config/components/predicateAnalysis-use-cmc-condition-differential.properties:ResourceLimitChecker.fromConfiguration, INFO)

Using predicate analysis with MathSAT5 version 5.5.4 (bd863fede57e) (Feb 21 2019 15:05:40, gmp 6.1.0, gcc 4.8.5, 64-bit, reentrant) and JFactory 1.21. (Analysis config/components/predicateAnalysis-use-cmc-condition-differential.properties:PredicateCPA:PredicateCPA.<init>, INFO)

Using refinement for predicate analysis with PredicateAbstractionRefinementStrategy strategy. (Analysis config/components/predicateAnalysis-use-cmc-condition-differential.properties:PredicateCPA:PredicateCPARefiner.<init>, INFO)

Starting analysis 2 ... (RestartAlgorithm.run, INFO)

Stopping analysis ... (CPAchecker.runAlgorithm, INFO)

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 28) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
Graphical representation included in the file "./output/Counterexample.2.html".
```

Command:

```shell
scripts/cpa.sh -differentialProgramGenerator -setprop differential.program=../examples/absSum.c \
  ../examples/absSumMod.c \
  -setprop residualprogram.strategy=CONDITION_PLUS_FOLD \
  -setprop residualprogram.folderType=CFA \
  -setprop differential.ignoreDeclarations=true \
  -spec ../examples/unreach-call.prp
```

Output:

```text
Running CPAchecker with default heap size (1200M). Specify a larger value with -heap if you have more RAM.
Running CPAchecker with default stack size (1024k). Specify a larger value with -stack if needed.
Language C detected and set for analysis (CPAMain.detectFrontendLanguageIfNecessary, INFO)

CPAchecker 1.9.1-svn-230d2ca59d / differentialProgramGenerator (OpenJDK 64-Bit Server VM 11.0.7) started (CPAchecker.run, INFO)

Parsing CFA from file(s) "../examples/absSumMod.c" (CPAchecker.parse, INFO)

Using Restarting Algorithm (CoreComponentsFactory.createAlgorithm, INFO)

The following configuration options were specified but are not used:
 differential.ignoreDeclarations
 residualprogram.strategy
 differential.program
 residualprogram.folderType 
 (CPAchecker.printConfigurationWarnings, WARNING)

Starting analysis ... (CPAchecker.runAlgorithm, INFO)

Loading analysis 1 from file config/components/differentialAutomatonGeneratorAsComponent.properties ... (RestartAlgorithm.run, INFO)

Mismatch of configuration options when loading from 'config/components/differentialAutomatonGeneratorAsComponent.properties': 'specification' has two values 'config/specification/sv-comp-reachability.spc' and '../specification/default.spc'. Using '../specification/default.spc'. (NestingAlgorithm.checkConfigs, INFO)

Starting analysis 1 ... (RestartAlgorithm.run, INFO)

Statistics for algorithm 1 of 2
===============================
Total time for algorithm 1:     0.995s

AutomatonAnalysis (ModificationsAutomaton) statistics
-----------------------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.004s
Total time for strengthen operator:                    0.136s
Automaton transfers with branching:                0
Automaton transfer successors:                     1.00 (sum: 23, count: 23, min: 1, max: 1) [1 x 23]
Number of states with assumption transitions:      0

AutomatonAnalysis (SVCOMP) statistics
-------------------------------------
Number of states:                                  1
Total time for successor computation:                  0.011s
Automaton transfers with branching:                0
Automaton transfer successors:                     0.96 (sum: 22, count: 23, min: 0, max: 1) [0, 1 x 22]
Number of states with assumption transitions:      0

CPA algorithm statistics
------------------------
Number of iterations:            22
Max size of waitlist:            3
Average size of waitlist:        1
Number of computed successors:   22
Max successors for one state:    2
Number of times merged:          0
Number of times stopped:         1
Number of times breaked:         0

Total time for CPA algorithm:         0.202s (Max:     0.202s)
  Time for choose from waitlist:      0.001s
  Time for precision adjustment:      0.019s
  Time for transfer relation:         0.168s
  Time for stop operator:             0.001s
  Time for adding to reached set:     0.000s

Assumption Collection algorithm statistics
------------------------------------------
Number of locations with assumptions:              1
Number of states in automaton:                     10

CPA algorithm statistics
------------------------
Number of iterations:            22
Max size of waitlist:            3
Average size of waitlist:        1
Number of computed successors:   22
Max successors for one state:    2
Number of times merged:          0
Number of times stopped:         1
Number of times breaked:         0

Total time for CPA algorithm:         0.202s (Max:     0.202s)
  Time for choose from waitlist:      0.001s
  Time for precision adjustment:      0.019s
  Time for transfer relation:         0.168s
  Time for stop operator:             0.001s
  Time for adding to reached set:     0.000s

Assumption Collection algorithm statistics
------------------------------------------
Number of locations with assumptions:              1
Number of states in automaton:                     20

RestartAlgorithm switches to the next configuration... (RestartAlgorithm.run, INFO)

Loading analysis 2 from file config/residualProgramGenerator.properties ... (RestartAlgorithm.run, INFO)

Mismatch of configuration options when loading from 'config/residualProgramGenerator.properties': 'specification' has two values 'config/specification/sv-comp-reachability.spc' and 'specification/default.spc'. Using 'specification/default.spc'. (NestingAlgorithm.checkConfigs, INFO)

Using the following resource limits: CPU-time limit of 900s (Analysis config/residualProgramGenerator.properties:ResourceLimitChecker.fromConfiguration, INFO)

Starting analysis 2 ... (RestartAlgorithm.run, INFO)

Start construction of residual program. (Analysis config/residualProgramGenerator.properties:ResidualProgramConstructionAlgorithm.run, INFO)

Write residual program to file. (Analysis config/residualProgramGenerator.properties:ResidualProgramConstructionAlgorithm.run, INFO)

Generate residual program (Analysis config/residualProgramGenerator.properties:ResidualProgramConstructionAlgorithm.writeResidualProgram, INFO)

Finished construction of residual program.  If the selected strategy is SLICING or COMBINATION, please continue with the slicing tool (Frama-C) (Analysis config/residualProgramGenerator.properties:ResidualProgramConstructionAlgorithm.run, INFO)

Analysis 2 terminated, but result is unsound. (RestartAlgorithm.run, INFO)

No further configuration available. (RestartAlgorithm.run, INFO)

Stopping analysis ... (CPAchecker.runAlgorithm, INFO)

Finished.
More details about the verification run can be found in the directory "./output".
Graphical representation included in the file "./output/Report.html".
```

Command:

```shell
scripts/cpa.sh -svcomp20 ../cpachecker/output/residual_program.c \
  -spec ../examples/unreach-call.prp \
  -timelimit 900
```

Output:

```text
Running CPAchecker with default heap size (1200M). Specify a larger value with -heap if you have more RAM.
Running CPAchecker with default stack size (1024k). Specify a larger value with -stack if needed.
Language C detected and set for analysis (CPAMain.detectFrontendLanguageIfNecessary, INFO)

Using the following resource limits: CPU-time limit of 900s (ResourceLimitChecker.fromConfiguration, INFO)

CPAchecker 1.9 / svcomp20 (OpenJDK 64-Bit Server VM 11.0.7) started (CPAchecker.run, INFO)

Parsing CFA from file(s) "../cpachecker/output/residual_program.c" (CPAchecker.parse, INFO)

Using heuristics to select analysis (CoreComponentsFactory.createAlgorithm, INFO)

The following configuration options were specified but are not used:
 cpa.callstack.unsupportedFunctions
 termination.violation.witness
 cpa.predicate.memoryAllocationsAlwaysSucceed
 cpa.arg.compressWitness
 cpa.callstack.skipFunctionPointerRecursion
 cpa.composite.aggregateBasicBlocks
 counterexample.export.graphml
 counterexample.export.compressWitness
 cpa.arg.proofWitness 
 (CPAchecker.printConfigurationWarnings, WARNING)

Starting analysis ... (CPAchecker.runAlgorithm, INFO)

Performing heuristic ... (SelectionAlgorithm.chooseConfig, INFO)

Using the following resource limits: CPU-time limit of 900s (Analysis config/components/configselection-restart-bmc-fallbacks.properties:ResourceLimitChecker.fromConfiguration, INFO)

Using Restarting Algorithm (Analysis config/components/configselection-restart-bmc-fallbacks.properties:CoreComponentsFactory.createAlgorithm, INFO)

Loading analysis 1 from file config/components/configselection-singleconfig-bmc.properties ... (Analysis config/components/configselection-restart-bmc-fallbacks.properties:RestartAlgorithm.run, INFO)

Mismatch of configuration options when loading from 'config/components/configselection-singleconfig-bmc.properties': 'limits.time.cpu' has two values '900' and '900s'. Using '900s'. (Analysis config/components/configselection-restart-bmc-fallbacks.properties:NestingAlgorithm.checkConfigs, INFO)

Mismatch of configuration options when loading from 'config/components/configselection-singleconfig-bmc.properties': 'specification' has two values 'config/specification/sv-comp-reachability.spc' and 'specification/default.spc'. Using 'specification/default.spc'. (Analysis config/components/configselection-restart-bmc-fallbacks.properties:NestingAlgorithm.checkConfigs, INFO)

Using the following resource limits: CPU-time limit of 900s (Analysis config/components/configselection-restart-bmc-fallbacks.properties:Analysis config/components/configselection-singleconfig-bmc.properties:ResourceLimitChecker.fromConfiguration, INFO)

Using predicate analysis with MathSAT5 version 5.5.4 (bd863fede57e) (Feb 21 2019 15:05:40, gmp 6.1.0, gcc 4.8.5, 64-bit, reentrant). (Analysis config/components/configselection-restart-bmc-fallbacks.properties:Analysis config/components/configselection-singleconfig-bmc.properties:PredicateCPA:PredicateCPA.<init>, INFO)

Starting analysis 1 ... (Analysis config/components/configselection-restart-bmc-fallbacks.properties:RestartAlgorithm.run, INFO)

Creating formula for program (Analysis config/components/configselection-restart-bmc-fallbacks.properties:Analysis config/components/configselection-singleconfig-bmc.properties:AbstractBMCAlgorithm.run, INFO)

Starting satisfiability check... (Analysis config/components/configselection-restart-bmc-fallbacks.properties:Analysis config/components/configselection-singleconfig-bmc.properties:AbstractBMCAlgorithm.boundedModelCheck, INFO)

Error found, creating error path (Analysis config/components/configselection-restart-bmc-fallbacks.properties:Analysis config/components/configselection-singleconfig-bmc.properties:BMCAlgorithm.analyzeCounterexample, INFO)

Stopping analysis ... (CPAchecker.runAlgorithm, INFO)

Verification result: FALSE. Property violation (unreach-call: __VERIFIER_error(); called in line 19) found by chosen configuration.
More details about the verification run can be found in the directory "./output".
Graphical representation included in the file "./output/Counterexample.1.html".
```

Command:

```shell
./Ultimate.py --file ../cpachecker/output/residual_program.c \
  --spec ../examples/unreach-call.prp --architecture 32bit
```

Output:

```
Checking for ERROR reachability
Using default analysis
Version f470102c
Calling Ultimate with: /usr/lib/jvm/java-8-openjdk-amd64/bin/java -Dosgi.configuration.area=/home/sosy/difference-data/uautomizer/data/config -Xmx12G -Xms1G -jar /home/sosy/difference-data/uautomizer/plugins/org.eclipse.equinox.launcher_1.3.100.v20150511-1540.jar -data @noDefault -ultimatedata /home/sosy/difference-data/uautomizer/data -tc /home/sosy/difference-data/uautomizer/config/AutomizerReach.xml -i ../cpachecker/output/residual_program.c -s /home/sosy/difference-data/uautomizer/config/svcomp-Reach-32bit-Automizer_Default.epf --cacsl2boogietranslator.entry.function main --witnessprinter.witness.directory /home/sosy/difference-data/uautomizer --witnessprinter.witness.filename witness.graphml --witnessprinter.write.witness.besides.input.file false --witnessprinter.graph.data.specification CHECK( init(main()), LTL(G ! call(__VERIFIER_error())) )

 --witnessprinter.graph.data.producer Automizer --witnessprinter.graph.data.architecture 32bit --witnessprinter.graph.data.programhash 5b8e1ba8862008a4674890ea51d8ecc5dc9ee385
...................................................................................................................................................................................................................................................................................................................................................................
Execution finished normally
Writing output log to file Ultimate.log
Result:
TRUE
```

## License

This artifact is licensed under the Apache 2.0 License with copyright by Dirk Beyer.
