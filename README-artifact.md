<!--
This file is part of the replication artifact for
difference verification with conditions:
https://gitlab.com/sosy-lab/research/data/difference-data

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Replication Package for Article "Difference Verification with Conditions", SEFM'20

This replication package supports reproducing the claims of the SEFM 2020 publication
"D. Beyer, M.-C. Jakobs, and T. Lemberger: Difference Verification with Conditions".

Difference verification with conditions is an incremental program
verification technique that can be integrated in software projects at any time.
It first applies a change analysis that detects which parts of a software have changed
between revisions and encodes that information in a condition.
Based on this condition, an off-the-shelf verifier is used to verify
only those parts of the software that are influenced by the changes.

This replication package contains a virtual machine (VM) with all components necessary to run
https://gitlab.com/sosy-lab/research/data/difference-data
in an offline setting.

VM username: sosy
VM password: sosy

The VM is intended for use with VirtualBox.
After importing `SEFM20-Difference-Verification.ova` into VirtualBox and starting it,
refer to `~/difference-data/README.md` for more information.

## License

Most parts of this artifact are licensed under the Apache 2.0 License with copyright by Dirk Beyer.

The VM runs Ubuntu 20.04. Ubuntu and its software components are licensed under various licenses.
Please refer to the individual component for license information.
