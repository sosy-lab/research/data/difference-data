# This file is part of the replication artifact for
# difference verification with conditions:
# https://gitlab.com/sosy-lab/research/data/difference-data
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

RESULTS_DIR ?= data_raw/
CPACHECKER_DIR ?= cpachecker
CPASEQ_DIR ?= cpa-seq
UAUTOMIZER_DIR ?= uautomizer

PRIORITY := "HIGH"
CLIENT_HEAP := 6000
CPU := "E3-1230"
CLOUD_OPTIONS=--no-container --cloud --cloudCPUModel $(CPU) --cloudClientHeap $(CLIENT_HEAP) --cloudPriority $(PRIORITY)
BENCHMARK_OPTIONS := -o ../$(RESULTS_DIR)
# Uncomment this line to run benchmarks on SoSy-Lab's cluster (only available for our own chair)
#BENCHMARK_OPTIONS := $(CLOUD_OPTIONS) -o ../$(RESULTS_DIR)

TIME ?= "2020-02-29 00:01"
TIME_ENC ?= 2020-02-29_0001

PREDICATE_COMBOS := predicate-combos

RUNDEF_FULL ?= fullAnalysis
RUNDEF_DIFF_RUN ?= diffAnalysis-ignoreDecl
RUNDEF_DIFF_GEN ?= diffProgram-ignoreDecl

BENCHMARK_SET ?= eca-token_ring gcd-newton pals-eca pcsfifo-token_ring square_softfloat

BENCHMARK_DEF_DIR := benchmark_defs

all: tasks verifiers-alone diff-predicate diff-reducer-combos precision-reuse-predicate precision-reuse-predicate-diff

DATE := $(shell date +%Y-%m-%d)
clean:
	mkdir -p data_backups/$(DATE)
	mv $(RESULTS_DIR)/* data_backups/$(DATE) | true
	rm -rf cpa-seq uautomizer

tools: cpa-seq/cpachecker.jar uautomizer/Ultimate.py cpachecker/cpachecker.jar

cpachecker/cpachecker.jar:
	git submodule update --init cpachecker
	cd cpachecker && \
		ant && ant clean build && ant jar

cpa-seq/cpachecker.jar:
	rm -rf cpa-seq
	wget https://gitlab.com/sosy-lab/sv-comp/archives-2020/-/raw/master/2020/cpa-seq.zip -O /tmp/cpa-seq.zip
	unzip /tmp/cpa-seq.zip
	mv CPAchecker-1.9-unix cpa-seq

uautomizer/Ultimate.py:
	rm -rf uautomizer
	wget https://gitlab.com/sosy-lab/sv-comp/archives-2020/-/raw/master/2020/uautomizer.zip -O /tmp/uautomizer.zip
	unzip /tmp/uautomizer.zip
	mv UAutomizer-linux/ uautomizer

$(RESULTS_DIR):
	mkdir -p $(RESULTS_DIR)

initial-selection: $(RESULTS_DIR)/cpa-seq-initial-selection.$(TIME_ENC).results.full.xml.bz2 $(RESULTS_DIR)/predicate-initial-selection.$(TIME_ENC).results.full.xml.bz2 $(RESULTS_DIR)/uautomizer-initial-selection.$(TIME_ENC).results.full.xml.bz2

verifiers-alone: predicate-analysis cpa-seq uautomizer

diff: diff-predicate cpa-seq-reducer uautomizer-reducer

predicate-analysis: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/$(PREDICATE_COMBOS)-$(set).$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2)

cpa-seq: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/cpa-seq-$(set).full.$(TIME_ENC).results.full.xml.bz2)

uautomizer: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/uautomizer-$(set).full.$(TIME_ENC).results.full.xml.bz2)

diff-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2)

diff-gen: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/$(PREDICATE_COMBOS)-$(set).$(RUNDEF_DIFF_GEN).$(TIME_ENC).results.$(RUNDEF_DIFF_GEN).xml.bz2)

diff-reducer-combos: cpa-seq-reducer uautomizer-reducer

cpa-seq-reducer: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/cpa-seq-$(set).residuals-ignored.$(TIME_ENC).results.residuals-ignored.xml.bz2)

uautomizer-reducer: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/uautomizer-$(set).residuals-ignored.$(TIME_ENC).results.residuals-ignored.xml.bz2)

precision-init-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/predicate-init-prec-$(set).$(TIME_ENC).results.xml.bz2)

precision-reuse-predicate: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/predicate-combos-prec-$(set).$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2)

precision-reuse-predicate-diff: $(foreach set, $(BENCHMARK_SET), $(RESULTS_DIR)/predicate-combos-prec-$(set).$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2)


include Makefile.tasks

$(RESULTS_DIR)/predicate-initial-selection.$(TIME_ENC).results.fullAnalysis.xml.bz2: benchmark_defs/predicate-initial-selection.xml $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) --startTime $(TIME) ../$<

$(RESULTS_DIR)/cpa-seq-initial-selection.$(TIME_ENC).results.full.xml.bz2: benchmark_defs/cpa-seq-initial-selection.xml cpa-seq/cpachecker.jar $(RESULTS_DIR)
	cd $(CPASEQ_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) --startTime $(TIME) ../$<

$(RESULTS_DIR)/uautomizer-initial-selection.$(TIME_ENC).results.full.xml.bz2: benchmark_defs/uautomizer-initial-selection.xml uautomizer/Ultimate.py $(RESULTS_DIR)
	cd $(UAUTOMIZER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) --startTime $(TIME) ../$<

$(RESULTS_DIR)/$(PREDICATE_COMBOS)-%.$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2: benchmark_defs/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_FULL) -n $(RUNDEF_FULL) --startTime $(TIME) ../$<

$(RESULTS_DIR)/$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFF_RUN) -n $(RUNDEF_DIFF_RUN) --startTime $(TIME) ../$<

$(RESULTS_DIR)/$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFF_GEN).$(TIME_ENC).results.$(RUNDEF_DIFF_GEN).xml.bz2: $(BENCHMARK_DEF_DIR)/$(PREDICATE_COMBOS)-%.xml $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFF_GEN) -n $(RUNDEF_DIFF_GEN) --startTime $(TIME) ../$<
	scripts/prepare_residual_tasks.py --benchmark-dir svcomp_combination_tasks/ $(RESULTS_DIR)/$(PREDICATE_COMBOS)-$(*F).$(RUNDEF_DIFF_GEN).$(TIME_ENC).files/

$(RESULTS_DIR)/cpa-seq-%.full.$(TIME_ENC).results.full.xml.bz2: $(BENCHMARK_DEF_DIR)/cpa-seq-%.xml cpa-seq/cpachecker.jar $(RESULTS_DIR)
	cd $(CPASEQ_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r full -n full --startTime $(TIME) ../$<

$(RESULTS_DIR)/uautomizer-%.full.$(TIME_ENC).results.full.xml.bz2: $(BENCHMARK_DEF_DIR)/uautomizer-%.xml uautomizer/Ultimate.py $(RESULTS_DIR)
	cd $(UAUTOMIZER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r full -n full --startTime $(TIME) ../$<

$(RESULTS_DIR)/cpa-seq-%.residuals-ignored.$(TIME_ENC).results.residuals-ignored.xml.bz2: $(BENCHMARK_DEF_DIR)/cpa-seq-%.xml $(RESULTS_DIR)/$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFF_GEN).$(TIME_ENC).results.$(RUNDEF_DIFF_GEN).xml.bz2 cpa-seq/cpachecker.jar $(RESULTS_DIR)
	cd $(CPASEQ_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r residuals-ignored -n residuals-ignored --startTime $(TIME) ../$<

$(RESULTS_DIR)/uautomizer-%.residuals-ignored.$(TIME_ENC).results.residuals-ignored.xml.bz2: $(BENCHMARK_DEF_DIR)/uautomizer-%.xml $(RESULTS_DIR)/$(PREDICATE_COMBOS)-%.$(RUNDEF_DIFF_GEN).$(TIME_ENC).results.$(RUNDEF_DIFF_GEN).xml.bz2 uautomizer/Ultimate.py $(RESULTS_DIR)
	cd $(UAUTOMIZER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r residuals-ignored -n residuals-ignored --startTime $(TIME) ../$<

$(RESULTS_DIR)/predicate-init-prec-%.$(TIME_ENC).results.xml.bz2: $(BENCHMARK_DEF_DIR)/predicate-init-prec-%.xml $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) --startTime $(TIME) ../$<
	scripts/move_precisions_from_results_to_target_folder.py $(RESULTS_DIR)/predicate-init-prec-%.$(TIME_ENC).files/

$(RESULTS_DIR)/predicate-combos-prec-%.$(RUNDEF_FULL).$(TIME_ENC).results.$(RUNDEF_FULL).xml.bz2: $(BENCHMARK_DEF_DIR)/predicate-combos-prec-%.xml $(RESULTS_DIR)/predicate-init-prec-%.$(TIME_ENC).results.xml.bz2 $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_FULL) -n $(RUNDEF_FULL) --startTime $(TIME) ../$<

$(RESULTS_DIR)/predicate-combos-prec-%.$(RUNDEF_DIFF_RUN).$(TIME_ENC).results.$(RUNDEF_DIFF_RUN).xml.bz2: $(BENCHMARK_DEF_DIR)/predicate-combos-prec-%.xml $(RESULTS_DIR)/predicate-init-prec-%.$(TIME_ENC).results.xml.bz2 $(RESULTS_DIR)
	cd $(CPACHECKER_DIR) && \
		../$(CPACHECKER_DIR)/scripts/benchmark.py $(BENCHMARK_OPTIONS) -r $(RUNDEF_DIFF_RUN) -n $(RUNDEF_DIFF_RUN) --startTime $(TIME) ../$<

