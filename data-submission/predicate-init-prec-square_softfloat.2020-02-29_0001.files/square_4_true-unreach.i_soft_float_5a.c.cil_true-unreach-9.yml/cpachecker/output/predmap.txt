(set-info :source |printed by MathSAT|)
(declare-fun |main1::result| () (_ FloatingPoint 8 24))

main1:
(assert (let ((.def_96 (fp.leq (fp #b0 #b00000000 #b00000000000000000000000) |main1::result|))).def_96))
(assert (let ((.def_101 (fp.lt |main1::result| (fp #b0 #b01111111 #b01100110000000000010101)))).def_101))

