(set-info :source |printed by MathSAT|)
(declare-fun |main1::result| () (_ FloatingPoint 8 24))

main1:
(assert (let ((.def_96 (fp.lt |main1::result| (fp #b0 #b01111111 #b10000000000000000000000)))).def_96))
(assert (let ((.def_101 (fp.leq (fp #b0 #b00000000 #b00000000000000000000000) |main1::result|))).def_101))

__VERIFIER_assert N24:
(assert false)

base2flt N39:
(assert false)

base2flt N56:
(assert false)

