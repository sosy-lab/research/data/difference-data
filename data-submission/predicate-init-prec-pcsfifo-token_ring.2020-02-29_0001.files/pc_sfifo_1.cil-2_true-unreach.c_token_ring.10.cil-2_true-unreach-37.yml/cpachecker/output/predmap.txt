(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun t5_pc () (_ BitVec 32))
(declare-fun t6_pc () (_ BitVec 32))
(declare-fun t7_pc () (_ BitVec 32))
(declare-fun t8_pc () (_ BitVec 32))
(declare-fun t9_pc () (_ BitVec 32))
(declare-fun t10_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun E_8 () (_ BitVec 32))
(declare-fun E_7 () (_ BitVec 32))
(declare-fun E_6 () (_ BitVec 32))
(declare-fun E_5 () (_ BitVec 32))
(declare-fun E_4 () (_ BitVec 32))
(declare-fun E_3 () (_ BitVec 32))
(declare-fun E_2 () (_ BitVec 32))
(declare-fun E_10 () (_ BitVec 32))
(declare-fun E_9 () (_ BitVec 32))
(declare-fun t5_st () (_ BitVec 32))
(declare-fun t7_st () (_ BitVec 32))
(declare-fun t8_st () (_ BitVec 32))
(declare-fun t6_st () (_ BitVec 32))
(declare-fun t3_st () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun t4_st () (_ BitVec 32))
(declare-fun t9_st () (_ BitVec 32))
(declare-fun t10_st () (_ BitVec 32))
(declare-fun t2_st () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))

*:
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))

error1 N4:
(assert false)
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))

do_write_p N64:
(assert (let ((.def_989 (= c_dr_pc (_ bv0 32)))).def_989))
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))
(assert (let ((.def_1092 (= q_free (_ bv1 32)))).def_1092))
(assert false)
(assert (let ((.def_1435 (= p_last_write q_buf_0))).def_1435))
(assert (let ((.def_1095 (= p_dw_pc (_ bv1 32)))).def_1095))
(assert (let ((.def_1624 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1625 (= p_num_write .def_1624))).def_1625)))
(assert (let ((.def_2516 (= q_free (_ bv0 32)))).def_2516))
(assert (let ((.def_1627 (= c_dr_pc (_ bv1 32)))).def_1627))
(assert (let ((.def_5317 (= q_read_ev (_ bv1 32)))).def_5317))

do_read_c N100:
(assert (let ((.def_1095 (= p_dw_pc (_ bv1 32)))).def_1095))
(assert (let ((.def_1092 (= q_free (_ bv1 32)))).def_1092))
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))
(assert (let ((.def_1435 (= p_last_write q_buf_0))).def_1435))
(assert (let ((.def_1624 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1625 (= p_num_write .def_1624))).def_1625)))
(assert (let ((.def_1627 (= c_dr_pc (_ bv1 32)))).def_1627))
(assert false)

eval1 N170:
(assert (let ((.def_989 (= c_dr_pc (_ bv0 32)))).def_989))
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))
(assert (let ((.def_1092 (= q_free (_ bv1 32)))).def_1092))
(assert (let ((.def_1365 (= p_dw_pc (_ bv0 32)))).def_1365))
(assert (let ((.def_1627 (= c_dr_pc (_ bv1 32)))).def_1627))
(assert (let ((.def_3939 (= c_dr_st (_ bv0 32)))).def_3939))
(assert (let ((.def_1095 (= p_dw_pc (_ bv1 32)))).def_1095))
(assert (let ((.def_5317 (= q_read_ev (_ bv1 32)))).def_5317))
(assert (let ((.def_5320 (= p_dw_st (_ bv0 32)))).def_5320))
(assert (let ((.def_1435 (= p_last_write q_buf_0))).def_1435))
(assert (let ((.def_1624 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1625 (= p_num_write .def_1624))).def_1625)))

start_simulation1 N221:
(assert (let ((.def_989 (= c_dr_pc (_ bv0 32)))).def_989))
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))
(assert (let ((.def_1092 (= q_free (_ bv1 32)))).def_1092))
(assert (let ((.def_1365 (= p_dw_pc (_ bv0 32)))).def_1365))
(assert (let ((.def_5317 (= q_read_ev (_ bv1 32)))).def_5317))

error2 N260:
(assert false)
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))

eval2 N880:
(assert (let ((.def_8103 (= m_pc (_ bv0 32)))).def_8103))
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))
(assert (let ((.def_17327 (= t1_pc (_ bv0 32)))).def_17327))
(assert (let ((.def_17330 (= t2_pc (_ bv0 32)))).def_17330))
(assert (let ((.def_17333 (= t3_pc (_ bv0 32)))).def_17333))
(assert (let ((.def_17336 (= t4_pc (_ bv0 32)))).def_17336))
(assert (let ((.def_17339 (= t5_pc (_ bv0 32)))).def_17339))
(assert (let ((.def_17342 (= t6_pc (_ bv0 32)))).def_17342))
(assert (let ((.def_17345 (= t7_pc (_ bv0 32)))).def_17345))
(assert (let ((.def_17348 (= t8_pc (_ bv0 32)))).def_17348))
(assert (let ((.def_17351 (= t9_pc (_ bv0 32)))).def_17351))
(assert (let ((.def_17354 (= t10_pc (_ bv0 32)))).def_17354))
(assert (let ((.def_17356 (= m_pc (_ bv1 32)))).def_17356))
(assert (let ((.def_17358 (= t1_pc (_ bv1 32)))).def_17358))
(assert (let ((.def_17360 (= t2_pc (_ bv1 32)))).def_17360))
(assert (let ((.def_17362 (= t3_pc (_ bv1 32)))).def_17362))
(assert (let ((.def_17364 (= t4_pc (_ bv1 32)))).def_17364))
(assert (let ((.def_17366 (= t5_pc (_ bv1 32)))).def_17366))
(assert (let ((.def_17368 (= t6_pc (_ bv1 32)))).def_17368))
(assert (let ((.def_17370 (= t7_pc (_ bv1 32)))).def_17370))
(assert (let ((.def_17372 (= t8_pc (_ bv1 32)))).def_17372))
(assert (let ((.def_17374 (= t9_pc (_ bv1 32)))).def_17374))
(assert (let ((.def_17376 (= t10_pc (_ bv1 32)))).def_17376))
(assert (let ((.def_17379 (= m_st (_ bv0 32)))).def_17379))
(assert (let ((.def_68932 (= E_M (_ bv1 32)))).def_68932))
(assert (let ((.def_68935 (= E_8 (_ bv1 32)))).def_68935))
(assert (let ((.def_68938 (= E_7 (_ bv1 32)))).def_68938))
(assert (let ((.def_68941 (= E_6 (_ bv1 32)))).def_68941))
(assert (let ((.def_68944 (= E_5 (_ bv1 32)))).def_68944))
(assert (let ((.def_68947 (= E_4 (_ bv1 32)))).def_68947))
(assert (let ((.def_68950 (= E_3 (_ bv1 32)))).def_68950))
(assert (let ((.def_68953 (= E_2 (_ bv1 32)))).def_68953))
(assert (let ((.def_68956 (= E_10 (_ bv1 32)))).def_68956))
(assert (let ((.def_68959 (= E_9 (_ bv1 32)))).def_68959))
(assert (let ((.def_68962 (= t5_st (_ bv0 32)))).def_68962))
(assert (let ((.def_68965 (= t7_st (_ bv0 32)))).def_68965))
(assert (let ((.def_68968 (= t8_st (_ bv0 32)))).def_68968))
(assert (let ((.def_68971 (= t6_st (_ bv0 32)))).def_68971))
(assert (let ((.def_68974 (= t3_st (_ bv0 32)))).def_68974))
(assert (let ((.def_68977 (= t1_st (_ bv0 32)))).def_68977))
(assert (let ((.def_68980 (= t4_st (_ bv0 32)))).def_68980))
(assert (let ((.def_68983 (= t9_st (_ bv0 32)))).def_68983))
(assert (let ((.def_68986 (= t10_st (_ bv0 32)))).def_68986))
(assert (let ((.def_68989 (= t2_st (_ bv0 32)))).def_68989))
(assert (let ((.def_68992 (= E_1 (_ bv2 32)))).def_68992))
(assert (let ((.def_68995 (bvadd (_ bv10 32) local)))(let ((.def_68997 (= .def_68995 token))).def_68997)))

start_simulation2 N1399:
(assert (let ((.def_8103 (= m_pc (_ bv0 32)))).def_8103))
(assert (let ((.def_645 (= c_last_read p_last_write))).def_645))
(assert (let ((.def_654 (= c_num_read p_num_write))).def_654))
(assert (let ((.def_17327 (= t1_pc (_ bv0 32)))).def_17327))
(assert (let ((.def_17330 (= t2_pc (_ bv0 32)))).def_17330))
(assert (let ((.def_17333 (= t3_pc (_ bv0 32)))).def_17333))
(assert (let ((.def_17336 (= t4_pc (_ bv0 32)))).def_17336))
(assert (let ((.def_17339 (= t5_pc (_ bv0 32)))).def_17339))
(assert (let ((.def_17342 (= t6_pc (_ bv0 32)))).def_17342))
(assert (let ((.def_17345 (= t7_pc (_ bv0 32)))).def_17345))
(assert (let ((.def_17348 (= t8_pc (_ bv0 32)))).def_17348))
(assert (let ((.def_17351 (= t9_pc (_ bv0 32)))).def_17351))
(assert (let ((.def_17354 (= t10_pc (_ bv0 32)))).def_17354))
(assert (let ((.def_17356 (= m_pc (_ bv1 32)))).def_17356))
(assert (let ((.def_17358 (= t1_pc (_ bv1 32)))).def_17358))
(assert (let ((.def_17360 (= t2_pc (_ bv1 32)))).def_17360))
(assert (let ((.def_17362 (= t3_pc (_ bv1 32)))).def_17362))
(assert (let ((.def_17364 (= t4_pc (_ bv1 32)))).def_17364))
(assert (let ((.def_17366 (= t5_pc (_ bv1 32)))).def_17366))
(assert (let ((.def_17368 (= t6_pc (_ bv1 32)))).def_17368))
(assert (let ((.def_17370 (= t7_pc (_ bv1 32)))).def_17370))
(assert (let ((.def_17372 (= t8_pc (_ bv1 32)))).def_17372))
(assert (let ((.def_17374 (= t9_pc (_ bv1 32)))).def_17374))
(assert (let ((.def_17376 (= t10_pc (_ bv1 32)))).def_17376))
(assert (let ((.def_68932 (= E_M (_ bv1 32)))).def_68932))
(assert (let ((.def_68935 (= E_8 (_ bv1 32)))).def_68935))
(assert (let ((.def_68938 (= E_7 (_ bv1 32)))).def_68938))
(assert (let ((.def_68941 (= E_6 (_ bv1 32)))).def_68941))
(assert (let ((.def_68944 (= E_5 (_ bv1 32)))).def_68944))
(assert (let ((.def_68947 (= E_4 (_ bv1 32)))).def_68947))
(assert (let ((.def_68950 (= E_3 (_ bv1 32)))).def_68950))
(assert (let ((.def_68953 (= E_2 (_ bv1 32)))).def_68953))
(assert (let ((.def_68956 (= E_10 (_ bv1 32)))).def_68956))
(assert (let ((.def_68959 (= E_9 (_ bv1 32)))).def_68959))
(assert (let ((.def_17379 (= m_st (_ bv0 32)))).def_17379))

