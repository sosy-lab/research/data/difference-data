(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))

*:
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))

error1 N4:
(assert false)
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))

eval1 N282:
(assert (let ((.def_837 (= c_dr_pc (_ bv0 32)))).def_837))
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))
(assert (let ((.def_1328 (= c_dr_pc (_ bv2 32)))).def_1328))
(assert (let ((.def_1331 (= c_dr_st (_ bv0 32)))).def_1331))

start_simulation1 N338:
(assert (let ((.def_837 (= c_dr_pc (_ bv0 32)))).def_837))
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))
(assert (let ((.def_1328 (= c_dr_pc (_ bv2 32)))).def_1328))

error2 N393:
(assert false)
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))

eval2 N702:
(assert (let ((.def_4355 (= m_pc (_ bv0 32)))).def_4355))
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))
(assert (let ((.def_6696 (= t1_pc (_ bv0 32)))).def_6696))
(assert (let ((.def_6699 (= t2_pc (_ bv0 32)))).def_6699))
(assert (let ((.def_6702 (= t3_pc (_ bv0 32)))).def_6702))
(assert (let ((.def_6705 (= t4_pc (_ bv0 32)))).def_6705))
(assert (let ((.def_6707 (= m_pc (_ bv1 32)))).def_6707))
(assert (let ((.def_6709 (= t1_pc (_ bv1 32)))).def_6709))
(assert (let ((.def_6711 (= t2_pc (_ bv1 32)))).def_6711))
(assert (let ((.def_6713 (= t3_pc (_ bv1 32)))).def_6713))
(assert (let ((.def_6715 (= t4_pc (_ bv1 32)))).def_6715))
(assert (let ((.def_6718 (= m_st (_ bv0 32)))).def_6718))

start_simulation2 N975:
(assert (let ((.def_4355 (= m_pc (_ bv0 32)))).def_4355))
(assert (let ((.def_677 (= c_last_read p_last_write))).def_677))
(assert (let ((.def_686 (= c_num_read p_num_write))).def_686))
(assert (let ((.def_6696 (= t1_pc (_ bv0 32)))).def_6696))
(assert (let ((.def_6699 (= t2_pc (_ bv0 32)))).def_6699))
(assert (let ((.def_6702 (= t3_pc (_ bv0 32)))).def_6702))
(assert (let ((.def_6705 (= t4_pc (_ bv0 32)))).def_6705))
(assert (let ((.def_6707 (= m_pc (_ bv1 32)))).def_6707))
(assert (let ((.def_6709 (= t1_pc (_ bv1 32)))).def_6709))
(assert (let ((.def_6711 (= t2_pc (_ bv1 32)))).def_6711))
(assert (let ((.def_6713 (= t3_pc (_ bv1 32)))).def_6713))
(assert (let ((.def_6715 (= t4_pc (_ bv1 32)))).def_6715))

