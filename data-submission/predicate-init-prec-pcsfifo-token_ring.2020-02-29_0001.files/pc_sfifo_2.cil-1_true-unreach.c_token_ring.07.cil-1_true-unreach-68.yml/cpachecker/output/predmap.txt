(set-info :source |printed by MathSAT|)
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun q_req_up () (_ BitVec 32))
(declare-fun q_write_ev () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun t5_pc () (_ BitVec 32))
(declare-fun t6_pc () (_ BitVec 32))
(declare-fun t7_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_7 () (_ BitVec 32))
(declare-fun E_5 () (_ BitVec 32))
(declare-fun E_4 () (_ BitVec 32))
(declare-fun E_3 () (_ BitVec 32))
(declare-fun E_2 () (_ BitVec 32))
(declare-fun E_6 () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun t5_st () (_ BitVec 32))
(declare-fun t2_st () (_ BitVec 32))
(declare-fun t3_st () (_ BitVec 32))
(declare-fun t6_st () (_ BitVec 32))
(declare-fun t7_st () (_ BitVec 32))
(declare-fun t4_st () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))

*:
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))

error1 N4:
(assert false)
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))

do_write_p N78:
(assert (let ((.def_785 (= c_dr_pc (_ bv0 32)))).def_785))
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))
(assert (let ((.def_853 (= q_free (_ bv1 32)))).def_853))
(assert false)
(assert (let ((.def_1037 (= p_last_write q_buf_0))).def_1037))
(assert (let ((.def_1177 (= c_dr_pc (_ bv1 32)))).def_1177))
(assert (let ((.def_1179 (= q_free (_ bv0 32)))).def_1179))
(assert (let ((.def_1183 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1184 (= p_num_write .def_1183))).def_1184)))
(assert (let ((.def_1865 (= c_dr_st (_ bv0 32)))).def_1865))
(assert (let ((.def_4265 (= q_req_up (_ bv1 32)))).def_4265))
(assert (let ((.def_5898 (= q_write_ev (_ bv1 32)))).def_5898))
(assert (let ((.def_5900 (= q_write_ev (_ bv0 32)))).def_5900))
(assert (let ((.def_8235 (= q_read_ev (_ bv1 32)))).def_8235))
(assert (let ((.def_8237 (= q_read_ev (_ bv0 32)))).def_8237))

do_read_c N112:
(assert (let ((.def_853 (= q_free (_ bv1 32)))).def_853))
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))
(assert (let ((.def_1037 (= p_last_write q_buf_0))).def_1037))
(assert (let ((.def_1183 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1184 (= p_num_write .def_1183))).def_1184)))
(assert false)
(assert (let ((.def_1179 (= q_free (_ bv0 32)))).def_1179))
(assert (let ((.def_5898 (= q_write_ev (_ bv1 32)))).def_5898))
(assert (let ((.def_5900 (= q_write_ev (_ bv0 32)))).def_5900))
(assert (let ((.def_1181 (= p_dw_pc (_ bv1 32)))).def_1181))
(assert (let ((.def_4265 (= q_req_up (_ bv1 32)))).def_4265))
(assert (let ((.def_2339 (= p_dw_st (_ bv0 32)))).def_2339))
(assert (let ((.def_8235 (= q_read_ev (_ bv1 32)))).def_8235))
(assert (let ((.def_8237 (= q_read_ev (_ bv0 32)))).def_8237))

eval1 N228:
(assert (let ((.def_785 (= c_dr_pc (_ bv0 32)))).def_785))
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))
(assert (let ((.def_853 (= q_free (_ bv1 32)))).def_853))
(assert (let ((.def_995 (= p_dw_pc (_ bv0 32)))).def_995))
(assert (let ((.def_1177 (= c_dr_pc (_ bv1 32)))).def_1177))
(assert (let ((.def_1179 (= q_free (_ bv0 32)))).def_1179))
(assert (let ((.def_1181 (= p_dw_pc (_ bv1 32)))).def_1181))
(assert (let ((.def_2339 (= p_dw_st (_ bv0 32)))).def_2339))
(assert (let ((.def_1037 (= p_last_write q_buf_0))).def_1037))
(assert (let ((.def_1183 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1184 (= p_num_write .def_1183))).def_1184)))
(assert (let ((.def_1865 (= c_dr_st (_ bv0 32)))).def_1865))
(assert (let ((.def_4265 (= q_req_up (_ bv1 32)))).def_4265))
(assert (let ((.def_5898 (= q_write_ev (_ bv1 32)))).def_5898))
(assert (let ((.def_5900 (= q_write_ev (_ bv0 32)))).def_5900))
(assert (let ((.def_8235 (= q_read_ev (_ bv1 32)))).def_8235))
(assert (let ((.def_8237 (= q_read_ev (_ bv0 32)))).def_8237))

start_simulation1 N283:
(assert (let ((.def_785 (= c_dr_pc (_ bv0 32)))).def_785))
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))
(assert (let ((.def_853 (= q_free (_ bv1 32)))).def_853))
(assert (let ((.def_995 (= p_dw_pc (_ bv0 32)))).def_995))
(assert (let ((.def_1177 (= c_dr_pc (_ bv1 32)))).def_1177))
(assert (let ((.def_1179 (= q_free (_ bv0 32)))).def_1179))
(assert (let ((.def_1181 (= p_dw_pc (_ bv1 32)))).def_1181))
(assert (let ((.def_1865 (= c_dr_st (_ bv0 32)))).def_1865))
(assert (let ((.def_2339 (= p_dw_st (_ bv0 32)))).def_2339))
(assert (let ((.def_1037 (= p_last_write q_buf_0))).def_1037))
(assert (let ((.def_5898 (= q_write_ev (_ bv1 32)))).def_5898))
(assert (let ((.def_5900 (= q_write_ev (_ bv0 32)))).def_5900))
(assert (let ((.def_8235 (= q_read_ev (_ bv1 32)))).def_8235))
(assert (let ((.def_8237 (= q_read_ev (_ bv0 32)))).def_8237))
(assert (let ((.def_1183 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1184 (= p_num_write .def_1183))).def_1184)))

error2 N328:
(assert false)
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))

eval2 N786:
(assert (let ((.def_11080 (= m_pc (_ bv0 32)))).def_11080))
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))
(assert (let ((.def_16267 (= t1_pc (_ bv0 32)))).def_16267))
(assert (let ((.def_16270 (= t2_pc (_ bv0 32)))).def_16270))
(assert (let ((.def_16273 (= t3_pc (_ bv0 32)))).def_16273))
(assert (let ((.def_16276 (= t4_pc (_ bv0 32)))).def_16276))
(assert (let ((.def_16279 (= t5_pc (_ bv0 32)))).def_16279))
(assert (let ((.def_16282 (= t6_pc (_ bv0 32)))).def_16282))
(assert (let ((.def_16285 (= t7_pc (_ bv0 32)))).def_16285))
(assert (let ((.def_16287 (= m_pc (_ bv1 32)))).def_16287))
(assert (let ((.def_16289 (= t1_pc (_ bv1 32)))).def_16289))
(assert (let ((.def_16291 (= t2_pc (_ bv1 32)))).def_16291))
(assert (let ((.def_16293 (= t3_pc (_ bv1 32)))).def_16293))
(assert (let ((.def_16295 (= t4_pc (_ bv1 32)))).def_16295))
(assert (let ((.def_16297 (= t5_pc (_ bv1 32)))).def_16297))
(assert (let ((.def_16299 (= t6_pc (_ bv1 32)))).def_16299))
(assert (let ((.def_16301 (= t7_pc (_ bv1 32)))).def_16301))
(assert (let ((.def_16304 (= m_st (_ bv0 32)))).def_16304))
(assert (let ((.def_26997 (= E_7 (_ bv1 32)))).def_26997))
(assert (let ((.def_27000 (= E_5 (_ bv1 32)))).def_27000))
(assert (let ((.def_27003 (= E_4 (_ bv1 32)))).def_27003))
(assert (let ((.def_27006 (= E_3 (_ bv1 32)))).def_27006))
(assert (let ((.def_27009 (= E_2 (_ bv1 32)))).def_27009))
(assert (let ((.def_27012 (= E_6 (_ bv1 32)))).def_27012))
(assert (let ((.def_27014 (= E_6 (_ bv2 32)))).def_27014))
(assert (let ((.def_27016 (= E_3 (_ bv2 32)))).def_27016))
(assert (let ((.def_27019 (= E_M (_ bv1 32)))).def_27019))
(assert (let ((.def_27022 (= t1_st (_ bv0 32)))).def_27022))
(assert (let ((.def_27025 (= t5_st (_ bv0 32)))).def_27025))
(assert (let ((.def_27028 (= t2_st (_ bv0 32)))).def_27028))
(assert (let ((.def_27031 (= t3_st (_ bv0 32)))).def_27031))
(assert (let ((.def_27034 (= t6_st (_ bv0 32)))).def_27034))
(assert (let ((.def_27037 (= t7_st (_ bv0 32)))).def_27037))
(assert (let ((.def_27040 (= t4_st (_ bv0 32)))).def_27040))
(assert (let ((.def_27043 (= E_1 (_ bv2 32)))).def_27043))
(assert (let ((.def_27046 (bvadd (_ bv7 32) local)))(let ((.def_27048 (= .def_27046 token))).def_27048)))
(assert (let ((.def_64557 (= E_5 (_ bv2 32)))).def_64557))
(assert (let ((.def_64559 (= E_4 (_ bv2 32)))).def_64559))
(assert (let ((.def_64561 (= E_1 (_ bv1 32)))).def_64561))
(assert (let ((.def_64563 (= t6_st (_ bv2 32)))).def_64563))
(assert (let ((.def_64565 (= t3_st (_ bv2 32)))).def_64565))
(assert (let ((.def_64567 (= t2_st (_ bv2 32)))).def_64567))
(assert (let ((.def_64569 (= m_st (_ bv2 32)))).def_64569))
(assert (let ((.def_64571 (bvadd (_ bv6 32) local)))(let ((.def_64572 (= token .def_64571))).def_64572)))
(assert (let ((.def_64574 (bvadd (_ bv5 32) local)))(let ((.def_64575 (= token .def_64574))).def_64575)))
(assert (let ((.def_64577 (bvadd (_ bv4 32) local)))(let ((.def_64578 (= token .def_64577))).def_64578)))
(assert (let ((.def_64580 (bvadd (_ bv3 32) local)))(let ((.def_64581 (= token .def_64580))).def_64581)))
(assert (let ((.def_64583 (bvadd (_ bv2 32) local)))(let ((.def_64584 (= token .def_64583))).def_64584)))
(assert (let ((.def_64586 (bvadd (_ bv1 32) local)))(let ((.def_64587 (= token .def_64586))).def_64587)))
(assert (let ((.def_64589 (= local token))).def_64589))

start_simulation2 N1182:
(assert (let ((.def_11080 (= m_pc (_ bv0 32)))).def_11080))
(assert (let ((.def_589 (= c_num_read p_num_write))).def_589))
(assert (let ((.def_598 (= c_last_read p_last_write))).def_598))
(assert (let ((.def_16267 (= t1_pc (_ bv0 32)))).def_16267))
(assert (let ((.def_16270 (= t2_pc (_ bv0 32)))).def_16270))
(assert (let ((.def_16273 (= t3_pc (_ bv0 32)))).def_16273))
(assert (let ((.def_16276 (= t4_pc (_ bv0 32)))).def_16276))
(assert (let ((.def_16279 (= t5_pc (_ bv0 32)))).def_16279))
(assert (let ((.def_16282 (= t6_pc (_ bv0 32)))).def_16282))
(assert (let ((.def_16285 (= t7_pc (_ bv0 32)))).def_16285))
(assert (let ((.def_16287 (= m_pc (_ bv1 32)))).def_16287))
(assert (let ((.def_16289 (= t1_pc (_ bv1 32)))).def_16289))
(assert (let ((.def_16291 (= t2_pc (_ bv1 32)))).def_16291))
(assert (let ((.def_16293 (= t3_pc (_ bv1 32)))).def_16293))
(assert (let ((.def_16295 (= t4_pc (_ bv1 32)))).def_16295))
(assert (let ((.def_16297 (= t5_pc (_ bv1 32)))).def_16297))
(assert (let ((.def_16299 (= t6_pc (_ bv1 32)))).def_16299))
(assert (let ((.def_16301 (= t7_pc (_ bv1 32)))).def_16301))
(assert (let ((.def_26997 (= E_7 (_ bv1 32)))).def_26997))
(assert (let ((.def_27000 (= E_5 (_ bv1 32)))).def_27000))
(assert (let ((.def_27003 (= E_4 (_ bv1 32)))).def_27003))
(assert (let ((.def_27006 (= E_3 (_ bv1 32)))).def_27006))
(assert (let ((.def_27009 (= E_2 (_ bv1 32)))).def_27009))
(assert (let ((.def_27012 (= E_6 (_ bv1 32)))).def_27012))
(assert (let ((.def_27014 (= E_6 (_ bv2 32)))).def_27014))
(assert (let ((.def_27016 (= E_3 (_ bv2 32)))).def_27016))
(assert (let ((.def_27019 (= E_M (_ bv1 32)))).def_27019))
(assert (let ((.def_16304 (= m_st (_ bv0 32)))).def_16304))
(assert (let ((.def_27028 (= t2_st (_ bv0 32)))).def_27028))
(assert (let ((.def_27031 (= t3_st (_ bv0 32)))).def_27031))
(assert (let ((.def_64557 (= E_5 (_ bv2 32)))).def_64557))
(assert (let ((.def_64559 (= E_4 (_ bv2 32)))).def_64559))
(assert (let ((.def_64561 (= E_1 (_ bv1 32)))).def_64561))
(assert (let ((.def_27022 (= t1_st (_ bv0 32)))).def_27022))

