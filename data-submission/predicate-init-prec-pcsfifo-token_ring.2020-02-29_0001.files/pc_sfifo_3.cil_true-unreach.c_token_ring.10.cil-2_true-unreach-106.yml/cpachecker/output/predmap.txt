(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun t5_pc () (_ BitVec 32))
(declare-fun t6_pc () (_ BitVec 32))
(declare-fun t7_pc () (_ BitVec 32))
(declare-fun t8_pc () (_ BitVec 32))
(declare-fun t9_pc () (_ BitVec 32))
(declare-fun t10_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_8 () (_ BitVec 32))
(declare-fun E_10 () (_ BitVec 32))
(declare-fun E_7 () (_ BitVec 32))
(declare-fun E_6 () (_ BitVec 32))
(declare-fun E_5 () (_ BitVec 32))
(declare-fun E_2 () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun E_9 () (_ BitVec 32))
(declare-fun t5_st () (_ BitVec 32))
(declare-fun t8_st () (_ BitVec 32))
(declare-fun E_4 () (_ BitVec 32))
(declare-fun E_3 () (_ BitVec 32))
(declare-fun t2_st () (_ BitVec 32))
(declare-fun t6_st () (_ BitVec 32))
(declare-fun t7_st () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun t9_st () (_ BitVec 32))
(declare-fun t10_st () (_ BitVec 32))
(declare-fun t3_st () (_ BitVec 32))
(declare-fun t4_st () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))

*:
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))

error1 N4:
(assert false)
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))

eval1 N282:
(assert (let ((.def_927 (= c_dr_pc (_ bv0 32)))).def_927))
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))
(assert (let ((.def_1418 (= c_dr_pc (_ bv2 32)))).def_1418))
(assert (let ((.def_1421 (= c_dr_st (_ bv0 32)))).def_1421))

start_simulation1 N338:
(assert (let ((.def_927 (= c_dr_pc (_ bv0 32)))).def_927))
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))
(assert (let ((.def_1418 (= c_dr_pc (_ bv2 32)))).def_1418))

error2 N393:
(assert false)
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))

eval2 N1013:
(assert (let ((.def_5135 (= m_pc (_ bv0 32)))).def_5135))
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))
(assert (let ((.def_14358 (= t1_pc (_ bv0 32)))).def_14358))
(assert (let ((.def_14361 (= t2_pc (_ bv0 32)))).def_14361))
(assert (let ((.def_14364 (= t3_pc (_ bv0 32)))).def_14364))
(assert (let ((.def_14367 (= t4_pc (_ bv0 32)))).def_14367))
(assert (let ((.def_14370 (= t5_pc (_ bv0 32)))).def_14370))
(assert (let ((.def_14373 (= t6_pc (_ bv0 32)))).def_14373))
(assert (let ((.def_14376 (= t7_pc (_ bv0 32)))).def_14376))
(assert (let ((.def_14379 (= t8_pc (_ bv0 32)))).def_14379))
(assert (let ((.def_14382 (= t9_pc (_ bv0 32)))).def_14382))
(assert (let ((.def_14385 (= t10_pc (_ bv0 32)))).def_14385))
(assert (let ((.def_14387 (= m_pc (_ bv1 32)))).def_14387))
(assert (let ((.def_14389 (= t1_pc (_ bv1 32)))).def_14389))
(assert (let ((.def_14391 (= t2_pc (_ bv1 32)))).def_14391))
(assert (let ((.def_14393 (= t3_pc (_ bv1 32)))).def_14393))
(assert (let ((.def_14395 (= t4_pc (_ bv1 32)))).def_14395))
(assert (let ((.def_14397 (= t5_pc (_ bv1 32)))).def_14397))
(assert (let ((.def_14399 (= t6_pc (_ bv1 32)))).def_14399))
(assert (let ((.def_14401 (= t7_pc (_ bv1 32)))).def_14401))
(assert (let ((.def_14403 (= t8_pc (_ bv1 32)))).def_14403))
(assert (let ((.def_14405 (= t9_pc (_ bv1 32)))).def_14405))
(assert (let ((.def_14407 (= t10_pc (_ bv1 32)))).def_14407))
(assert (let ((.def_14410 (= m_st (_ bv0 32)))).def_14410))
(assert (let ((.def_66650 (= E_8 (_ bv2 32)))).def_66650))
(assert (let ((.def_66653 (= E_10 (_ bv1 32)))).def_66653))
(assert (let ((.def_66655 (= E_8 (_ bv1 32)))).def_66655))
(assert (let ((.def_66658 (= E_7 (_ bv1 32)))).def_66658))
(assert (let ((.def_66661 (= E_6 (_ bv1 32)))).def_66661))
(assert (let ((.def_66664 (= E_5 (_ bv1 32)))).def_66664))
(assert (let ((.def_66667 (= E_2 (_ bv1 32)))).def_66667))
(assert (let ((.def_66670 (= E_M (_ bv1 32)))).def_66670))
(assert (let ((.def_66673 (= E_9 (_ bv1 32)))).def_66673))
(assert (let ((.def_66676 (= t5_st (_ bv0 32)))).def_66676))
(assert (let ((.def_66679 (= t8_st (_ bv0 32)))).def_66679))
(assert (let ((.def_66682 (= E_4 (_ bv1 32)))).def_66682))
(assert (let ((.def_66685 (= E_3 (_ bv1 32)))).def_66685))
(assert (let ((.def_66688 (= t2_st (_ bv0 32)))).def_66688))
(assert (let ((.def_66691 (= t6_st (_ bv0 32)))).def_66691))
(assert (let ((.def_66694 (= t7_st (_ bv0 32)))).def_66694))
(assert (let ((.def_66697 (= t1_st (_ bv0 32)))).def_66697))
(assert (let ((.def_66700 (= t9_st (_ bv0 32)))).def_66700))
(assert (let ((.def_66703 (= t10_st (_ bv0 32)))).def_66703))
(assert (let ((.def_66706 (= t3_st (_ bv0 32)))).def_66706))
(assert (let ((.def_66709 (= t4_st (_ bv0 32)))).def_66709))
(assert (let ((.def_66712 (= E_1 (_ bv1 32)))).def_66712))
(assert (let ((.def_66715 (bvadd (_ bv10 32) local)))(let ((.def_66717 (= .def_66715 token))).def_66717)))

start_simulation2 N1532:
(assert (let ((.def_5135 (= m_pc (_ bv0 32)))).def_5135))
(assert (let ((.def_767 (= c_last_read p_last_write))).def_767))
(assert (let ((.def_776 (= c_num_read p_num_write))).def_776))
(assert (let ((.def_14358 (= t1_pc (_ bv0 32)))).def_14358))
(assert (let ((.def_14361 (= t2_pc (_ bv0 32)))).def_14361))
(assert (let ((.def_14364 (= t3_pc (_ bv0 32)))).def_14364))
(assert (let ((.def_14367 (= t4_pc (_ bv0 32)))).def_14367))
(assert (let ((.def_14370 (= t5_pc (_ bv0 32)))).def_14370))
(assert (let ((.def_14373 (= t6_pc (_ bv0 32)))).def_14373))
(assert (let ((.def_14376 (= t7_pc (_ bv0 32)))).def_14376))
(assert (let ((.def_14379 (= t8_pc (_ bv0 32)))).def_14379))
(assert (let ((.def_14382 (= t9_pc (_ bv0 32)))).def_14382))
(assert (let ((.def_14385 (= t10_pc (_ bv0 32)))).def_14385))
(assert (let ((.def_14387 (= m_pc (_ bv1 32)))).def_14387))
(assert (let ((.def_14389 (= t1_pc (_ bv1 32)))).def_14389))
(assert (let ((.def_14391 (= t2_pc (_ bv1 32)))).def_14391))
(assert (let ((.def_14393 (= t3_pc (_ bv1 32)))).def_14393))
(assert (let ((.def_14395 (= t4_pc (_ bv1 32)))).def_14395))
(assert (let ((.def_14397 (= t5_pc (_ bv1 32)))).def_14397))
(assert (let ((.def_14399 (= t6_pc (_ bv1 32)))).def_14399))
(assert (let ((.def_14401 (= t7_pc (_ bv1 32)))).def_14401))
(assert (let ((.def_14403 (= t8_pc (_ bv1 32)))).def_14403))
(assert (let ((.def_14405 (= t9_pc (_ bv1 32)))).def_14405))
(assert (let ((.def_14407 (= t10_pc (_ bv1 32)))).def_14407))
(assert (let ((.def_66650 (= E_8 (_ bv2 32)))).def_66650))
(assert (let ((.def_66653 (= E_10 (_ bv1 32)))).def_66653))
(assert (let ((.def_66655 (= E_8 (_ bv1 32)))).def_66655))
(assert (let ((.def_66658 (= E_7 (_ bv1 32)))).def_66658))
(assert (let ((.def_66661 (= E_6 (_ bv1 32)))).def_66661))
(assert (let ((.def_66664 (= E_5 (_ bv1 32)))).def_66664))
(assert (let ((.def_66667 (= E_2 (_ bv1 32)))).def_66667))
(assert (let ((.def_66670 (= E_M (_ bv1 32)))).def_66670))
(assert (let ((.def_66673 (= E_9 (_ bv1 32)))).def_66673))
(assert (let ((.def_66676 (= t5_st (_ bv0 32)))).def_66676))
(assert (let ((.def_66679 (= t8_st (_ bv0 32)))).def_66679))
(assert (let ((.def_66682 (= E_4 (_ bv1 32)))).def_66682))
(assert (let ((.def_66685 (= E_3 (_ bv1 32)))).def_66685))
(assert (let ((.def_66688 (= t2_st (_ bv0 32)))).def_66688))
(assert (let ((.def_66691 (= t6_st (_ bv0 32)))).def_66691))
(assert (let ((.def_14410 (= m_st (_ bv0 32)))).def_14410))

