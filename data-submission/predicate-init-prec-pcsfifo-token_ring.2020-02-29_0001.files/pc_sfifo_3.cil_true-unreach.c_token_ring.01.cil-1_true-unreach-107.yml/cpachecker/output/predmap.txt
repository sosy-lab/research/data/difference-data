(set-info :source |printed by MathSAT|)
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))

*:
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))

error1 N4:
(assert false)
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))

eval1 N282:
(assert (let ((.def_792 (= c_dr_pc (_ bv0 32)))).def_792))
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))
(assert (let ((.def_1283 (= c_dr_pc (_ bv2 32)))).def_1283))
(assert (let ((.def_1286 (= c_dr_st (_ bv0 32)))).def_1286))

start_simulation1 N338:
(assert (let ((.def_792 (= c_dr_pc (_ bv0 32)))).def_792))
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))
(assert (let ((.def_1283 (= c_dr_pc (_ bv2 32)))).def_1283))

error2 N393:
(assert false)
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))

eval2 N527:
(assert (let ((.def_3928 (= m_pc (_ bv0 32)))).def_3928))
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))
(assert (let ((.def_4484 (= t1_pc (_ bv0 32)))).def_4484))
(assert (let ((.def_4486 (= m_pc (_ bv1 32)))).def_4486))
(assert (let ((.def_4488 (= t1_pc (_ bv1 32)))).def_4488))
(assert (let ((.def_4491 (= m_st (_ bv0 32)))).def_4491))
(assert (let ((.def_5209 (= E_M (_ bv1 32)))).def_5209))
(assert (let ((.def_5212 (= t1_st (_ bv0 32)))).def_5212))
(assert (let ((.def_5215 (bvadd (_ bv1 32) local)))(let ((.def_5217 (= .def_5215 token))).def_5217)))
(assert (let ((.def_6223 (= local token))).def_6223))
(assert (let ((.def_6226 (= E_1 (_ bv1 32)))).def_6226))
(assert (let ((.def_7532 (= E_M (_ bv0 32)))).def_7532))
(assert (let ((.def_7534 (= E_1 (_ bv0 32)))).def_7534))

start_simulation2 N677:
(assert (let ((.def_3928 (= m_pc (_ bv0 32)))).def_3928))
(assert (let ((.def_632 (= c_num_read p_num_write))).def_632))
(assert (let ((.def_641 (= c_last_read p_last_write))).def_641))
(assert (let ((.def_4484 (= t1_pc (_ bv0 32)))).def_4484))
(assert (let ((.def_4486 (= m_pc (_ bv1 32)))).def_4486))
(assert (let ((.def_4488 (= t1_pc (_ bv1 32)))).def_4488))
(assert (let ((.def_5209 (= E_M (_ bv1 32)))).def_5209))
(assert (let ((.def_5212 (= t1_st (_ bv0 32)))).def_5212))
(assert (let ((.def_4491 (= m_st (_ bv0 32)))).def_4491))
(assert (let ((.def_7532 (= E_M (_ bv0 32)))).def_7532))

