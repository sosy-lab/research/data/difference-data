(set-info :source |printed by MathSAT|)
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))

*:
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))

error1 N4:
(assert false)
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))

do_write_p N64:
(assert (let ((.def_869 (= c_dr_pc (_ bv0 32)))).def_869))
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))
(assert (let ((.def_972 (= q_free (_ bv1 32)))).def_972))
(assert false)
(assert (let ((.def_1315 (= p_last_write q_buf_0))).def_1315))
(assert (let ((.def_975 (= p_dw_pc (_ bv1 32)))).def_975))
(assert (let ((.def_1504 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1505 (= p_num_write .def_1504))).def_1505)))
(assert (let ((.def_2396 (= q_free (_ bv0 32)))).def_2396))
(assert (let ((.def_1507 (= c_dr_pc (_ bv1 32)))).def_1507))
(assert (let ((.def_5197 (= q_read_ev (_ bv1 32)))).def_5197))

do_read_c N100:
(assert (let ((.def_975 (= p_dw_pc (_ bv1 32)))).def_975))
(assert (let ((.def_972 (= q_free (_ bv1 32)))).def_972))
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))
(assert (let ((.def_1315 (= p_last_write q_buf_0))).def_1315))
(assert (let ((.def_1504 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1505 (= p_num_write .def_1504))).def_1505)))
(assert (let ((.def_1507 (= c_dr_pc (_ bv1 32)))).def_1507))
(assert false)

eval1 N170:
(assert (let ((.def_869 (= c_dr_pc (_ bv0 32)))).def_869))
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))
(assert (let ((.def_972 (= q_free (_ bv1 32)))).def_972))
(assert (let ((.def_1245 (= p_dw_pc (_ bv0 32)))).def_1245))
(assert (let ((.def_1507 (= c_dr_pc (_ bv1 32)))).def_1507))
(assert (let ((.def_3819 (= c_dr_st (_ bv0 32)))).def_3819))
(assert (let ((.def_975 (= p_dw_pc (_ bv1 32)))).def_975))
(assert (let ((.def_5197 (= q_read_ev (_ bv1 32)))).def_5197))
(assert (let ((.def_5200 (= p_dw_st (_ bv0 32)))).def_5200))
(assert (let ((.def_1315 (= p_last_write q_buf_0))).def_1315))
(assert (let ((.def_1504 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1505 (= p_num_write .def_1504))).def_1505)))

start_simulation1 N221:
(assert (let ((.def_869 (= c_dr_pc (_ bv0 32)))).def_869))
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))
(assert (let ((.def_972 (= q_free (_ bv1 32)))).def_972))
(assert (let ((.def_1245 (= p_dw_pc (_ bv0 32)))).def_1245))
(assert (let ((.def_5197 (= q_read_ev (_ bv1 32)))).def_5197))

error2 N260:
(assert false)
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))

eval2 N461:
(assert (let ((.def_7055 (= m_pc (_ bv0 32)))).def_7055))
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))
(assert (let ((.def_8105 (= t1_pc (_ bv0 32)))).def_8105))
(assert (let ((.def_8108 (= t2_pc (_ bv0 32)))).def_8108))
(assert (let ((.def_8110 (= m_pc (_ bv1 32)))).def_8110))
(assert (let ((.def_8112 (= t1_pc (_ bv1 32)))).def_8112))
(assert (let ((.def_8114 (= t2_pc (_ bv1 32)))).def_8114))
(assert (let ((.def_8117 (= m_st (_ bv0 32)))).def_8117))

start_simulation2 N652:
(assert (let ((.def_7055 (= m_pc (_ bv0 32)))).def_7055))
(assert (let ((.def_525 (= c_last_read p_last_write))).def_525))
(assert (let ((.def_534 (= c_num_read p_num_write))).def_534))
(assert (let ((.def_8105 (= t1_pc (_ bv0 32)))).def_8105))
(assert (let ((.def_8108 (= t2_pc (_ bv0 32)))).def_8108))
(assert (let ((.def_8110 (= m_pc (_ bv1 32)))).def_8110))
(assert (let ((.def_8112 (= t1_pc (_ bv1 32)))).def_8112))
(assert (let ((.def_8114 (= t2_pc (_ bv1 32)))).def_8114))

