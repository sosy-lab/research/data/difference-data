(set-info :source |printed by MathSAT|)
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun t2_pc () (_ BitVec 32))
(declare-fun t3_pc () (_ BitVec 32))
(declare-fun t4_pc () (_ BitVec 32))
(declare-fun t5_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))

*:
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))

error1 N4:
(assert false)
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))

eval1 N282:
(assert (let ((.def_852 (= c_dr_pc (_ bv0 32)))).def_852))
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))
(assert (let ((.def_1343 (= c_dr_pc (_ bv2 32)))).def_1343))
(assert (let ((.def_1346 (= c_dr_st (_ bv0 32)))).def_1346))

start_simulation1 N338:
(assert (let ((.def_852 (= c_dr_pc (_ bv0 32)))).def_852))
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))
(assert (let ((.def_1343 (= c_dr_pc (_ bv2 32)))).def_1343))

error2 N393:
(assert false)
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))

eval2 N756:
(assert (let ((.def_4489 (= m_pc (_ bv0 32)))).def_4489))
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))
(assert (let ((.def_7666 (= t1_pc (_ bv0 32)))).def_7666))
(assert (let ((.def_7669 (= t2_pc (_ bv0 32)))).def_7669))
(assert (let ((.def_7672 (= t3_pc (_ bv0 32)))).def_7672))
(assert (let ((.def_7675 (= t4_pc (_ bv0 32)))).def_7675))
(assert (let ((.def_7678 (= t5_pc (_ bv0 32)))).def_7678))
(assert (let ((.def_7680 (= m_pc (_ bv1 32)))).def_7680))
(assert (let ((.def_7682 (= t1_pc (_ bv1 32)))).def_7682))
(assert (let ((.def_7684 (= t2_pc (_ bv1 32)))).def_7684))
(assert (let ((.def_7686 (= t3_pc (_ bv1 32)))).def_7686))
(assert (let ((.def_7688 (= t4_pc (_ bv1 32)))).def_7688))
(assert (let ((.def_7690 (= t5_pc (_ bv1 32)))).def_7690))
(assert (let ((.def_7693 (= m_st (_ bv0 32)))).def_7693))

start_simulation2 N1070:
(assert (let ((.def_4489 (= m_pc (_ bv0 32)))).def_4489))
(assert (let ((.def_692 (= c_num_read p_num_write))).def_692))
(assert (let ((.def_701 (= c_last_read p_last_write))).def_701))
(assert (let ((.def_7666 (= t1_pc (_ bv0 32)))).def_7666))
(assert (let ((.def_7669 (= t2_pc (_ bv0 32)))).def_7669))
(assert (let ((.def_7672 (= t3_pc (_ bv0 32)))).def_7672))
(assert (let ((.def_7675 (= t4_pc (_ bv0 32)))).def_7675))
(assert (let ((.def_7678 (= t5_pc (_ bv0 32)))).def_7678))
(assert (let ((.def_7680 (= m_pc (_ bv1 32)))).def_7680))
(assert (let ((.def_7682 (= t1_pc (_ bv1 32)))).def_7682))
(assert (let ((.def_7684 (= t2_pc (_ bv1 32)))).def_7684))
(assert (let ((.def_7686 (= t3_pc (_ bv1 32)))).def_7686))
(assert (let ((.def_7688 (= t4_pc (_ bv1 32)))).def_7688))
(assert (let ((.def_7690 (= t5_pc (_ bv1 32)))).def_7690))

