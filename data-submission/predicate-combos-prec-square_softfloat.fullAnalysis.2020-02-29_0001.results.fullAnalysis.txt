   BENCHMARK INFORMATION
benchmark definition:    ../benchmark_defs/predicate-combos-prec-square_softfloat.xml
name:                    predicate-combos-prec-square_softfloat.fullAnalysis
run sets:                fullAnalysis
date:                    Sat, 2020-02-29 00:01:00
tool:                    CPAchecker 1.9.1-svn-32864
tool executable:         scripts/cpa.sh
options:                 -heap 10000M -benchmark -setprop cpa.predicate.abstraction.initialPredicates.applyFunctionWide=true
property file:           ../sv-benchmarks/c/properties/unreach-call.prp
resource limits:
- memory:                15000.0 MB
- time:                  900 s
- cpu cores:             4
hardware requirements:
- cpu model:             E3-1230
- cpu cores:             4
- memory:                15000.0 MB
------------------------------------------------------------



fullAnalysis
Run set 1 of 3 with options '-heap 10000M -benchmark -setprop cpa.predicate.abstraction.initialPredicates.applyFunctionWide=true -predicateAnalysis' and propertyfile '../sv-benchmarks/c/properties/unreach-call.prp'

inputfile                                                             status                    cpu time   wall time        host
--------------------------------------------------------------------------------------------------------------------------------
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-0.yml        TIMEOUT                     901.90      897.93  apollon074
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-0.yml        TIMEOUT                     901.87      897.95  apollon156
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-0.yml        TIMEOUT                     901.96      898.19  apollon104
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-0.yml        TIMEOUT                     902.01      882.73  apollon039
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-0.yml        true                        288.42      265.78  apollon063
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-1.yml        TIMEOUT                     901.80      898.19  apollon121
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-1.yml        TIMEOUT                     902.00      898.31  apollon029
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-1.yml        TIMEOUT                     901.99      897.86  apollon116
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-1.yml        TIMEOUT                     901.55      878.78  apollon099
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-1.yml        true                        299.59      273.93  apollon011
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-2.yml        TIMEOUT                     901.94      898.05  apollon002
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-2.yml        TIMEOUT                     902.02      897.99  apollon033
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-2.yml        TIMEOUT                     901.90      897.76  apollon107
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-2.yml        TIMEOUT                     901.94      897.88  apollon134
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-2.yml        true                        330.68      301.94  apollon012
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-3.yml        TIMEOUT                     901.89      898.02  apollon087
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-3.yml        TIMEOUT                     902.02      897.98  apollon149
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-3.yml        TIMEOUT                     902.06      897.86  apollon008
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-3.yml        TIMEOUT                     902.08      898.29  apollon149
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-3.yml        true                        300.33      275.33  apollon076
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-4.yml        TIMEOUT                     901.92      898.45  apollon044
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-4.yml        TIMEOUT                     901.90      897.85  apollon134
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-4.yml        TIMEOUT                     901.92      897.88  apollon112
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-4.yml        TIMEOUT                     902.07      889.76  apollon098
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-4.yml        true                        307.78      281.99  apollon167
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-5.yml        TIMEOUT                     902.05      897.96  apollon164
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-5.yml        TIMEOUT                     901.93      897.82  apollon100
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-5.yml        TIMEOUT                     901.92      897.70  apollon028
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-5.yml        TIMEOUT                     901.97      880.26  apollon081
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-5.yml        true                        333.58      305.63  apollon108
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-6.yml        TIMEOUT                     901.93      898.13  apollon089
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-6.yml        TIMEOUT                     901.93      897.94  apollon024
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-6.yml        TIMEOUT                     901.90      897.77  apollon133
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-6.yml        TIMEOUT                     901.90      897.81  apollon030
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-6.yml        true                        313.02      287.37  apollon127
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-7.yml        TIMEOUT                     901.90      897.96  apollon051
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-7.yml        TIMEOUT                     901.94      898.16  apollon109
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-7.yml        TIMEOUT                     902.06      897.83  apollon124
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-7.yml        TIMEOUT                     901.93      897.73  apollon044
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-7.yml        true                        331.21      296.67  apollon042
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-8.yml        TIMEOUT                     901.91      897.90  apollon130
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-8.yml        TIMEOUT                     901.91      897.93  apollon071
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-8.yml        TIMEOUT                     901.95      898.04  apollon030
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-8.yml        TIMEOUT                     902.03      897.95  apollon116
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-8.yml        true                        319.01      292.90  apollon086
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-9.yml        TIMEOUT                     901.85      898.11  apollon018
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-9.yml        TIMEOUT                     901.93      897.87  apollon146
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-9.yml        TIMEOUT                     901.98      898.22  apollon104
square_4_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-10.yml    TIMEOUT                     902.09      897.81  apollon080
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-10.yml       TIMEOUT                     901.81      897.90  apollon011
square_4_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-10.yml    TIMEOUT                     901.93      897.69  apollon075
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-10.yml       TIMEOUT                     901.95      897.76  apollon089
square_1_false-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml      false(unreach-call)          16.82       13.30  apollon137
square_2_false-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml      false(unreach-call)          91.41       87.89  apollon063
square_3_false-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml      false(unreach-call)         427.10      423.26  apollon090
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml       TIMEOUT                     901.94      898.02  apollon101
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml       TIMEOUT                     901.94      897.92  apollon136
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml       TIMEOUT                     901.51      882.09  apollon072
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-10.yml       true                        307.10      284.65  apollon099
square_4_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-11.yml    TIMEOUT                     902.04      897.99  apollon066
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-11.yml       TIMEOUT                     901.90      898.00  apollon139
square_4_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-11.yml    TIMEOUT                     902.03      898.08  apollon066
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-11.yml       TIMEOUT                     901.91      898.20  apollon151
square_1_false-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml      false(unreach-call)          16.54       13.17  apollon166
square_2_false-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml      false(unreach-call)          92.50       89.03  apollon159
square_3_false-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml      false(unreach-call)         431.72      427.74  apollon105
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml       TIMEOUT                     901.90      897.75  apollon130
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml       TIMEOUT                     901.94      898.04  apollon070
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml       TIMEOUT                     901.97      879.92  apollon166
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-11.yml       true                        269.13      244.79  apollon111
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-12.yml       TIMEOUT                     902.04      897.79  apollon017
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-12.yml       TIMEOUT                     901.91      897.87  apollon005
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-12.yml       TIMEOUT                     902.02      897.88  apollon024
square_4_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-13.yml    TIMEOUT                     901.96      898.04  apollon079
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-13.yml       TIMEOUT                     901.88      897.98  apollon117
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-13.yml       TIMEOUT                     902.08      898.13  apollon035
square_4_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-13.yml    TIMEOUT                     901.95      897.90  apollon048
square_1_false-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml      false(unreach-call)          17.10       13.33  apollon031
square_2_false-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml      false(unreach-call)          91.16       87.74  apollon073
square_3_false-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml      false(unreach-call)         426.21      422.51  apollon098
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml       TIMEOUT                     902.01      898.13  apollon154
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml       TIMEOUT                     901.97      897.94  apollon154
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml       TIMEOUT                     901.91      897.77  apollon136
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-13.yml       true                        312.43      286.32  apollon014
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-14.yml       TIMEOUT                     901.83      897.83  apollon107
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-14.yml       TIMEOUT                     902.03      898.01  apollon070
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-14.yml       TIMEOUT                     901.87      898.07  apollon140
square_5_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-15.yml    TIMEOUT                     901.99      897.97  apollon002
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-15.yml       TIMEOUT                     901.91      897.89  apollon060
square_5_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-15.yml    TIMEOUT                     901.87      897.86  apollon075
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-15.yml       TIMEOUT                     901.96      897.86  apollon143
square_1_false-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml      false(unreach-call)          16.56       13.13  apollon111
square_2_false-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml      false(unreach-call)          90.42       86.91  apollon148
square_3_false-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml      false(unreach-call)         390.29      386.55  apollon119
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml       TIMEOUT                     901.95      898.15  apollon128
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml       TIMEOUT                     902.13      897.96  apollon083
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml       TIMEOUT                     902.04      897.71  apollon164
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-15.yml       true                        327.47      302.54  apollon029
square_5_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-16.yml    TIMEOUT                     901.84      897.89  apollon095
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-16.yml       TIMEOUT                     901.93      897.93  apollon092
square_5_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-16.yml    TIMEOUT                     902.00      897.60  apollon051
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-16.yml       TIMEOUT                     901.93      897.91  apollon036
square_1_false-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml      false(unreach-call)          16.76       13.34  apollon031
square_2_false-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml      false(unreach-call)          94.20       90.47  apollon145
square_3_false-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml      false(unreach-call)         430.77      427.02  apollon146
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml       TIMEOUT                     902.02      897.75  apollon122
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml       TIMEOUT                     901.98      897.99  apollon108
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml       TIMEOUT                     901.90      898.13  apollon110
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-16.yml       true                        329.60      300.18  apollon027
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-17.yml       TIMEOUT                     901.91      897.77  apollon025
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-17.yml       TIMEOUT                     901.82      897.61  apollon021
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-17.yml       TIMEOUT                     902.01      897.93  apollon008
square_5_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-18.yml    TIMEOUT                     901.98      897.99  apollon015
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-18.yml       TIMEOUT                     902.00      897.88  apollon092
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-18.yml       TIMEOUT                     901.94      897.90  apollon027
square_5_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-18.yml    TIMEOUT                     902.04      897.89  apollon078
square_1_false-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml      false(unreach-call)          17.10       13.41  apollon157
square_2_false-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml      false(unreach-call)          94.04       90.14  apollon145
square_3_false-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml      false(unreach-call)         434.96      431.27  apollon125
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml       TIMEOUT                     901.93      898.06  apollon112
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml       TIMEOUT                     901.91      897.78  apollon096
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml       TIMEOUT                     901.99      898.00  apollon033
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-18.yml       true                        322.81      297.30  apollon155
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-19.yml       TIMEOUT                     901.95      897.77  apollon090
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-19.yml       TIMEOUT                     901.88      897.81  apollon142
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-19.yml       TIMEOUT                     901.97      897.94  apollon071
square_6_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-20.yml    TIMEOUT                     902.00      898.03  apollon054
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-20.yml       TIMEOUT                     901.90      897.97  apollon096
square_6_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-20.yml    TIMEOUT                     901.97      897.52  apollon022
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-20.yml       TIMEOUT                     901.94      897.93  apollon020
square_1_false-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml      false(unreach-call)          16.72       13.29  apollon153
square_2_false-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml      false(unreach-call)          91.69       88.33  apollon160
square_3_false-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml      false(unreach-call)         387.76      384.07  apollon069
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml       TIMEOUT                     902.02      898.03  apollon080
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml       TIMEOUT                     901.97      898.15  apollon160
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml       TIMEOUT                     902.11      885.03  apollon159
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-20.yml       true                        313.11      285.97  apollon161
square_6_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-21.yml    TIMEOUT                     902.10      898.01  apollon032
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-21.yml       TIMEOUT                     902.02      897.98  apollon113
square_6_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-21.yml    TIMEOUT                     902.02      897.83  apollon015
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-21.yml       TIMEOUT                     901.88      897.74  apollon038
square_1_false-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml      false(unreach-call)          16.70       13.21  apollon139
square_2_false-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml      false(unreach-call)          91.04       87.38  apollon156
square_3_false-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml      false(unreach-call)         425.50      421.91  apollon087
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml       TIMEOUT                     901.92      898.04  apollon048
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml       TIMEOUT                     902.04      898.07  apollon125
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml       TIMEOUT                     901.60      881.12  apollon167
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-21.yml       true                        302.72      278.22  apollon076
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-22.yml       TIMEOUT                     902.01      898.01  apollon084
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-22.yml       TIMEOUT                     901.90      898.02  apollon152
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-22.yml       TIMEOUT                     901.89      897.59  apollon034
square_6_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-23.yml    TIMEOUT                     902.05      897.98  apollon093
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-23.yml       TIMEOUT                     901.90      897.80  apollon037
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-23.yml       TIMEOUT                     901.97      898.13  apollon042
square_6_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-23.yml    TIMEOUT                     901.95      898.00  apollon034
square_1_false-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml      false(unreach-call)          16.78       13.41  apollon102
square_2_false-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml      false(unreach-call)          92.33       88.68  apollon018
square_3_false-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml      false(unreach-call)         432.82      428.92  apollon025
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml       TIMEOUT                     901.88      898.01  apollon057
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml       TIMEOUT                     901.95      897.96  apollon012
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml       TIMEOUT                     902.86      890.96  apollon009
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-23.yml       true                        327.39      300.89  apollon122
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-24.yml       TIMEOUT                     901.96      897.93  apollon078
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-24.yml       TIMEOUT                     902.01      897.75  apollon041
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-24.yml       TIMEOUT                     901.79      881.35  apollon114
square_7_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-25.yml    TIMEOUT                     901.96      897.73  apollon006
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-25.yml       TIMEOUT                     901.97      897.76  apollon152
square_7_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-25.yml    TIMEOUT                     901.97      897.88  apollon133
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-25.yml       TIMEOUT                     902.05      897.79  apollon022
square_1_false-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml      false(unreach-call)          16.54       13.18  apollon047
square_2_false-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml      false(unreach-call)          92.25       88.99  apollon074
square_3_false-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml      false(unreach-call)         376.08      372.49  apollon137
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml       TIMEOUT                     901.81      898.02  apollon026
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml       TIMEOUT                     902.07      897.98  apollon093
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml       TIMEOUT                     901.95      897.85  apollon127
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-25.yml       true                        315.15      289.37  apollon021
square_7_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-26.yml    TIMEOUT                     901.93      897.93  apollon121
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-26.yml       TIMEOUT                     901.86      897.88  apollon140
square_7_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-26.yml    TIMEOUT                     901.99      897.67  apollon142
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-26.yml       TIMEOUT                     902.07      897.91  apollon035
square_1_false-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml      false(unreach-call)          16.42       12.97  apollon114
square_2_false-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml      false(unreach-call)          89.93       86.49  apollon026
square_3_false-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml      false(unreach-call)         367.48      363.77  apollon148
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml       TIMEOUT                     901.85      897.81  apollon041
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml       TIMEOUT                     902.09      898.11  apollon153
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml       TIMEOUT                     901.96      898.01  apollon161
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-26.yml       true                        309.21      281.83  apollon045
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-27.yml       TIMEOUT                     901.86      897.86  apollon006
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-27.yml       TIMEOUT                     902.01      897.90  apollon110
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-27.yml       TIMEOUT                     902.12      897.81  apollon023
square_7_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-28.yml    TIMEOUT                     901.90      897.92  apollon105
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-28.yml       TIMEOUT                     901.76      880.15  apollon086
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-28.yml       TIMEOUT                     901.96      897.83  apollon095
square_7_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-28.yml    TIMEOUT                     902.00      897.80  apollon124
square_1_false-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml      false(unreach-call)          17.19       13.32  apollon037
square_2_false-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml      false(unreach-call)          83.30       79.66  apollon157
square_3_false-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml      false(unreach-call)         400.65      396.89  apollon131
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml       TIMEOUT                     901.97      898.05  apollon083
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml       TIMEOUT                     901.97      897.79  apollon143
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml       TIMEOUT                     902.01      897.98  apollon113
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-28.yml       true                        276.39      253.83  apollon102
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-29.yml       true                        323.94      295.89  apollon131
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-29.yml       true                        303.33      274.72  apollon117
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-29.yml       true                        318.29      292.21  apollon020
square_8_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-30.yml    false(unreach-call)         156.36      151.68  apollon119
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-30.yml       true                        311.50      284.60  apollon017
square_8_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-30.yml    false(unreach-call)         137.66      133.42  apollon059
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-30.yml       true                        299.95      275.97  apollon158
square_1_false-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml      false(unreach-call)          16.36       12.84  apollon128
square_2_false-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml      false(unreach-call)          92.81       89.04  apollon163
square_3_false-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml      false(unreach-call)         418.27      414.59  apollon039
square_4_true-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml       TIMEOUT                     901.85      897.86  apollon014
square_5_true-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml       TIMEOUT                     901.88      897.88  apollon005
square_6_true-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml       TIMEOUT                     901.98      897.73  apollon155
square_7_true-unreach.i_soft_float_2a.c.cil_true-unreach-30.yml       TIMEOUT                     901.99      897.73  apollon100
square_8_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-31.yml    false(unreach-call)         155.76      151.05  apollon060
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-31.yml       true                        295.89      271.75  apollon003
square_8_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-31.yml    false(unreach-call)         154.07      149.86  apollon003
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-31.yml       true                        312.10      284.87  apollon069
square_1_false-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml      false(unreach-call)          16.84       13.29  apollon059
square_2_false-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml      false(unreach-call)          87.96       84.50  apollon057
square_3_false-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml      false(unreach-call)         375.82      372.29  apollon047
square_4_true-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml       TIMEOUT                     901.96      897.68  apollon036
square_5_true-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml       TIMEOUT                     901.91      898.02  apollon163
square_6_true-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml       TIMEOUT                     901.90      897.93  apollon028
square_7_true-unreach.i_soft_float_3a.c.cil_true-unreach-31.yml       TIMEOUT                     901.85      897.57  apollon038
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-32.yml       true                        335.46      306.46  apollon009
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-32.yml       true                        304.51      279.40  apollon072
square_8_true-unreach.i_soft_float_5a.c.cil_true-unreach-32.yml       true                        297.51      274.34  apollon158
square_8_true-unreach.i_soft_float_1-3a.c.cil_false-unreach-33.yml    false(unreach-call)         158.61      153.58  apollon101
square_8_true-unreach.i_soft_float_2a.c.cil_true-unreach-33.yml       true                        302.05      278.37  apollon079
square_8_true-unreach.i_soft_float_3a.c.cil_true-unreach-33.yml       true                        323.27      295.69  apollon032
square_8_true-unreach.i_soft_float_4-3a.c.cil_false-unreach-33.yml    false(unreach-call)         157.11      153.00  apollon084
square_1_false-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml      false(unreach-call)          16.71       13.16  apollon081
square_2_false-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml      false(unreach-call)          92.79       89.10  apollon054
square_3_false-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml      false(unreach-call)         389.39      385.85  apollon045
square_4_true-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml       TIMEOUT                     902.00      898.05  apollon073
square_5_true-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml       TIMEOUT                     901.90      897.75  apollon109
square_6_true-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml       TIMEOUT                     902.03      897.82  apollon023
square_7_true-unreach.i_soft_float_5a.c.cil_true-unreach-33.yml       TIMEOUT                     901.93      897.94  apollon151
--------------------------------------------------------------------------------------------------------------------------------
Run set 1                                                             done                          None      933.92           -


diffAnalysis
Run set 2 of 3: skipped 


diffAnalysis-ignoreDecl
Run set 3 of 3: skipped 

Statistics:            240 Files
  correct:              84
    correct true:       33
    correct false:      51
  incorrect:             0
    incorrect true:      0
    incorrect false:     0
  unknown:             156
  Score:               117 (max: 405)
