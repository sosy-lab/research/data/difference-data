(set-info :source |printed by MathSAT|)
(declare-fun |__VERIFIER_assert::cond| () (_ BitVec 32))
(declare-fun |gcd_test::b| () (_ BitVec 8))
(declare-fun |main1::y| () (_ BitVec 8))
(declare-fun |gcd_test::a| () (_ BitVec 8))

__VERIFIER_assert:
(assert (let ((.def_210 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_210))

__VERIFIER_assert N6:
(assert false)
(assert (let ((.def_210 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_210))

gcd_test N18:
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_236 (bvslt (_ bv0 32) .def_235))).def_236)))
(assert (let ((.def_238 (= |gcd_test::b| (_ bv0 8)))).def_238))
(assert (let ((.def_424 (= |gcd_test::b| |main1::y|))).def_424))
(assert (let ((.def_427 (= |main1::y| |gcd_test::a|))).def_427))
(assert (let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_884 (bvslt .def_883 .def_882))).def_884)))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_886 (bvslt (_ bv0 32) .def_883))).def_886)))))
(assert (let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_888 ((_ extract 31 31) .def_882)))(let ((.def_889 (= .def_888 (_ bv1 1)))).def_889))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_891 ((_ extract 7 0) .def_883)))(let ((.def_892 (= .def_891 (_ bv0 8)))).def_892))))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_891 ((_ extract 7 0) .def_883)))(let ((.def_894 ((_ sign_extend 24) .def_891)))(let ((.def_895 (bvsrem .def_235 .def_894)))(let ((.def_896 ((_ extract 7 0) .def_895)))(let ((.def_897 (= .def_896 (_ bv0 8)))).def_897)))))))))
(assert (let ((.def_899 ((_ sign_extend 24) |main1::y|)))(let ((.def_900 (bvslt (_ bv0 32) .def_899))).def_900)))
(assert (let ((.def_899 ((_ sign_extend 24) |main1::y|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_902 (bvsrem .def_882 .def_899)))(let ((.def_903 (= .def_902 (_ bv0 32)))).def_903)))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_891 ((_ extract 7 0) .def_883)))(let ((.def_894 ((_ sign_extend 24) .def_891)))(let ((.def_895 (bvsrem .def_235 .def_894)))(let ((.def_905 ((_ extract 31 31) .def_895)))(let ((.def_906 (= .def_905 (_ bv1 1)))).def_906)))))))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_891 ((_ extract 7 0) .def_883)))(let ((.def_894 ((_ sign_extend 24) .def_891)))(let ((.def_895 (bvsrem .def_235 .def_894)))(let ((.def_908 (bvslt .def_895 .def_894))).def_908))))))))
(assert (let ((.def_899 ((_ sign_extend 24) |main1::y|)))(let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_891 ((_ extract 7 0) .def_883)))(let ((.def_894 ((_ sign_extend 24) .def_891)))(let ((.def_910 (= .def_894 .def_899))).def_910))))))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_912 ((_ extract 31 31) .def_883)))(let ((.def_913 (= .def_912 (_ bv1 1)))).def_913))))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_915 (bvslt .def_882 .def_883))).def_915)))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_883 (bvsrem .def_882 .def_235)))(let ((.def_917 (bvslt .def_883 .def_235))).def_917)))))
(assert (let ((.def_899 ((_ sign_extend 24) |main1::y|)))(let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_919 (= .def_235 .def_899))).def_919))))
(assert (let ((.def_235 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_921 ((_ extract 31 31) .def_235)))(let ((.def_922 (= .def_921 (_ bv1 1)))).def_922))))
(assert (let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_924 (bvslt (_ bv0 32) .def_882))).def_924)))
(assert (let ((.def_899 ((_ sign_extend 24) |main1::y|)))(let ((.def_882 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_926 (= .def_882 .def_899))).def_926))))

