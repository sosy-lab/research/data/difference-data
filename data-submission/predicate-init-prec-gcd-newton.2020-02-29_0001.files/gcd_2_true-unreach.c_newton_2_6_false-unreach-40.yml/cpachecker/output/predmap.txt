(set-info :source |printed by MathSAT|)
(declare-fun |__VERIFIER_assert::cond| () (_ BitVec 32))
(declare-fun |main1::y| () (_ BitVec 8))
(declare-fun |gcd_test::b| () (_ BitVec 8))
(declare-fun |gcd_test::a| () (_ BitVec 8))

__VERIFIER_assert:
(assert (let ((.def_168 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_168))

__VERIFIER_assert N6:
(assert false)
(assert (let ((.def_168 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_168))

gcd_test N18:
(assert (let ((.def_197 ((_ sign_extend 24) |main1::y|)))(let ((.def_198 (bvneg .def_197)))(let ((.def_199 ((_ extract 7 0) .def_198)))(let ((.def_201 (= .def_199 |gcd_test::b|))).def_201)))))
(assert (let ((.def_203 (= |main1::y| |gcd_test::b|))).def_203))
(assert (let ((.def_501 (= |main1::y| |gcd_test::a|))).def_501))
(assert (let ((.def_197 ((_ sign_extend 24) |main1::y|)))(let ((.def_198 (bvneg .def_197)))(let ((.def_199 ((_ extract 7 0) .def_198)))(let ((.def_503 (= .def_199 |gcd_test::a|))).def_503)))))
(assert (let ((.def_836 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_837 ((_ extract 31 31) .def_836)))(let ((.def_838 (= .def_837 (_ bv1 1)))).def_838))))
(assert (let ((.def_197 ((_ sign_extend 24) |main1::y|)))(let ((.def_840 ((_ extract 31 31) .def_197)))(let ((.def_841 (= .def_840 (_ bv1 1)))).def_841))))
(assert (let ((.def_843 (bvult |gcd_test::b| (_ bv128 8)))).def_843))
(assert (let ((.def_836 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_197 ((_ sign_extend 24) |main1::y|)))(let ((.def_845 (bvslt .def_197 .def_836))).def_845))))
(assert (let ((.def_197 ((_ sign_extend 24) |main1::y|)))(let ((.def_847 (bvslt (_ bv0 32) .def_197))).def_847)))
(assert (let ((.def_836 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_849 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_850 (bvsrem .def_849 .def_836)))(let ((.def_851 ((_ extract 7 0) .def_850)))(let ((.def_852 (= .def_851 (_ bv0 8)))).def_852))))))
(assert (let ((.def_854 (bvult |gcd_test::a| (_ bv128 8)))).def_854))
(assert (let ((.def_849 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_197 ((_ sign_extend 24) |main1::y|)))(let ((.def_856 (bvslt .def_197 .def_849))).def_856))))
(assert (let ((.def_858 (= |gcd_test::b| (_ bv0 8)))).def_858))

