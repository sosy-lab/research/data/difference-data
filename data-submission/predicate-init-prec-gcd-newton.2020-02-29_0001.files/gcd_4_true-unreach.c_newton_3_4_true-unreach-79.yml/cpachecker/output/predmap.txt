(set-info :source |printed by MathSAT|)
(declare-fun |__VERIFIER_assert::cond| () (_ BitVec 32))
(declare-fun |gcd_test::b| () (_ BitVec 32))
(declare-fun |gcd_test::a| () (_ BitVec 32))
(declare-fun |main1::x| () (_ BitVec 32))
(declare-fun |main1::y| () (_ BitVec 32))

__VERIFIER_assert:
(assert (let ((.def_173 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_173))

__VERIFIER_assert N6:
(assert false)
(assert (let ((.def_173 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_173))

gcd_test N20:
(assert (let ((.def_200 (= |gcd_test::b| (_ bv4294967278 32)))).def_200))
(assert (let ((.def_202 (= |gcd_test::b| (_ bv0 32)))).def_202))
(assert (let ((.def_309 (= |gcd_test::b| (_ bv18 32)))).def_309))
(assert (let ((.def_312 (= |gcd_test::a| (_ bv63 32)))).def_312))
(assert (let ((.def_314 (= |gcd_test::b| (_ bv4294967251 32)))).def_314))
(assert (let ((.def_663 (= |gcd_test::a| (_ bv45 32)))).def_663))
(assert (let ((.def_665 (= |gcd_test::b| (_ bv4294967269 32)))).def_665))
(assert (let ((.def_1125 (= |gcd_test::a| (_ bv27 32)))).def_1125))
(assert (let ((.def_1127 (= |gcd_test::b| (_ bv4294967287 32)))).def_1127))
(assert (let ((.def_1785 (= |gcd_test::a| (_ bv9 32)))).def_1785))
(assert (let ((.def_1787 (= |gcd_test::b| (_ bv9 32)))).def_1787))
(assert (let ((.def_2103 (= |main1::x| (_ bv63 32)))).def_2103))
(assert (let ((.def_2105 (= |gcd_test::a| (_ bv4294967287 32)))).def_2105))
(assert (let ((.def_2107 (= |gcd_test::b| (_ bv27 32)))).def_2107))
(assert (let ((.def_2588 (= |main1::y| (_ bv18 32)))).def_2588))
(assert (let ((.def_2590 (bvneg |gcd_test::a|)))(let ((.def_2591 (bvadd (_ bv18 32) .def_2590)))(let ((.def_2592 (= |gcd_test::b| .def_2591))).def_2592))))

