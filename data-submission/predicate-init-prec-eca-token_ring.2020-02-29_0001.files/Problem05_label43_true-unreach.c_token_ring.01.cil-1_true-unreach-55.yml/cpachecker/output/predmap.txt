(set-info :source |printed by MathSAT|)
(declare-fun a20 () (_ BitVec 32))
(declare-fun a25 () (_ BitVec 32))
(declare-fun a12 () (_ BitVec 32))
(declare-fun a15 () (_ BitVec 32))
(declare-fun a16 () (_ BitVec 32))
(declare-fun a2 () (_ BitVec 32))
(declare-fun a27 () (_ BitVec 32))
(declare-fun a9 () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))
(declare-fun E_M () (_ BitVec 32))
(declare-fun t1_st () (_ BitVec 32))
(declare-fun local () (_ BitVec 32))
(declare-fun token () (_ BitVec 32))
(declare-fun E_1 () (_ BitVec 32))

*:
(assert (let ((.def_107553 (= a20 (_ bv5 32)))).def_107553))

calculate_output8 N30053:
(assert false)
(assert (let ((.def_107553 (= a20 (_ bv5 32)))).def_107553))

main1 N30136:
(assert (let ((.def_107553 (= a20 (_ bv5 32)))).def_107553))
(assert (let ((.def_155105 (= a25 (_ bv1 32)))).def_155105))
(assert (let ((.def_155108 (= a12 (_ bv4 32)))).def_155108))
(assert (let ((.def_155111 (= a15 (_ bv1 32)))).def_155111))
(assert (let ((.def_155114 (= a16 (_ bv1 32)))).def_155114))
(assert (let ((.def_155117 (= a2 (_ bv10 32)))).def_155117))
(assert (let ((.def_155120 (= a27 (_ bv1 32)))).def_155120))
(assert (let ((.def_155123 (= a9 (_ bv18 32)))).def_155123))
(assert (let ((.def_321288 (= a20 (_ bv6 32)))).def_321288))
(assert (let ((.def_321290 (= a12 (_ bv3 32)))).def_321290))
(assert (let ((.def_321292 (= a12 (_ bv6 32)))).def_321292))
(assert (let ((.def_321294 (= a2 (_ bv9 32)))).def_321294))
(assert (let ((.def_321296 (= a9 (_ bv16 32)))).def_321296))
(assert (let ((.def_321298 (= a2 (_ bv12 32)))).def_321298))
(assert (let ((.def_321300 (= a2 (_ bv11 32)))).def_321300))
(assert (let ((.def_321302 (= a9 (_ bv17 32)))).def_321302))
(assert (let ((.def_321304 (= a12 (_ bv2 32)))).def_321304))
(assert (let ((.def_321306 (= a2 (_ bv13 32)))).def_321306))

error N30154:
(assert false)
(assert (let ((.def_107553 (= a20 (_ bv5 32)))).def_107553))

eval N30288:
(assert (let ((.def_746267 (= m_pc (_ bv0 32)))).def_746267))
(assert (let ((.def_107553 (= a20 (_ bv5 32)))).def_107553))
(assert (let ((.def_746820 (= t1_pc (_ bv0 32)))).def_746820))
(assert (let ((.def_746822 (= m_pc (_ bv1 32)))).def_746822))
(assert (let ((.def_746824 (= t1_pc (_ bv1 32)))).def_746824))
(assert (let ((.def_746827 (= m_st (_ bv0 32)))).def_746827))
(assert (let ((.def_747540 (= E_M (_ bv1 32)))).def_747540))
(assert (let ((.def_747543 (= t1_st (_ bv0 32)))).def_747543))
(assert (let ((.def_747546 (bvadd (_ bv1 32) local)))(let ((.def_747548 (= .def_747546 token))).def_747548)))
(assert (let ((.def_748548 (= local token))).def_748548))
(assert (let ((.def_748551 (= E_1 (_ bv1 32)))).def_748551))
(assert (let ((.def_749849 (= E_M (_ bv0 32)))).def_749849))
(assert (let ((.def_749851 (= E_1 (_ bv0 32)))).def_749851))

start_simulation N30438:
(assert (let ((.def_746267 (= m_pc (_ bv0 32)))).def_746267))
(assert (let ((.def_107553 (= a20 (_ bv5 32)))).def_107553))
(assert (let ((.def_746820 (= t1_pc (_ bv0 32)))).def_746820))
(assert (let ((.def_746822 (= m_pc (_ bv1 32)))).def_746822))
(assert (let ((.def_746824 (= t1_pc (_ bv1 32)))).def_746824))
(assert (let ((.def_747540 (= E_M (_ bv1 32)))).def_747540))
(assert (let ((.def_747543 (= t1_st (_ bv0 32)))).def_747543))
(assert (let ((.def_746827 (= m_st (_ bv0 32)))).def_746827))
(assert (let ((.def_749849 (= E_M (_ bv0 32)))).def_749849))

