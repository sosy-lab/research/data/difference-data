(set-info :source |printed by MathSAT|)
(declare-fun |main1::result| () (_ FloatingPoint 8 24))
(declare-fun |base2flt::m| () (_ BitVec 32))
(declare-fun |base2flt::e| () (_ BitVec 32))
(declare-fun |main2::zero| () (_ BitVec 32))

main1:
(assert (let ((.def_96 (fp.leq (fp #b0 #b00000000 #b00000000000000000000000) |main1::result|))).def_96))
(assert (let ((.def_101 (fp.lt |main1::result| (fp #b0 #b01111111 #b01100110011001100110011)))).def_101))

__VERIFIER_assert N24:
(assert false)

base2flt N39:
(assert false)
(assert (let ((.def_5061 (= |base2flt::m| (_ bv1 32)))).def_5061))
(assert (let ((.def_5064 (= |base2flt::e| (_ bv0 32)))).def_5064))
(assert (let ((.def_6979 (= |main2::zero| (_ bv0 32)))).def_6979))
(assert (let ((.def_7205 (= |base2flt::e| (_ bv4294967295 32)))).def_7205))
(assert (let ((.def_7207 (= |base2flt::m| (_ bv2 32)))).def_7207))
(assert (let ((.def_9473 (= |base2flt::e| (_ bv4294967294 32)))).def_9473))
(assert (let ((.def_9475 (= |base2flt::m| (_ bv4 32)))).def_9475))
(assert (let ((.def_12205 (= |base2flt::e| (_ bv4294967293 32)))).def_12205))
(assert (let ((.def_12207 (= |base2flt::m| (_ bv8 32)))).def_12207))
(assert (let ((.def_15495 (= |base2flt::e| (_ bv4294967292 32)))).def_15495))
(assert (let ((.def_15497 (= |base2flt::m| (_ bv16 32)))).def_15497))
(assert (let ((.def_19452 (= |base2flt::e| (_ bv4294967291 32)))).def_19452))
(assert (let ((.def_19454 (= |base2flt::m| (_ bv32 32)))).def_19454))
(assert (let ((.def_24177 (= |base2flt::e| (_ bv4294967290 32)))).def_24177))
(assert (let ((.def_24179 (= |base2flt::m| (_ bv64 32)))).def_24179))

base2flt N56:
(assert false)

