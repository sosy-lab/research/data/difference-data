(set-info :source |printed by MathSAT|)
(declare-fun |__VERIFIER_assert::cond| () (_ BitVec 32))
(declare-fun |gcd_test::a| () (_ BitVec 8))
(declare-fun |main1::x| () (_ BitVec 8))
(declare-fun |gcd_test::b| () (_ BitVec 8))

__VERIFIER_assert:
(assert (let ((.def_168 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_168))

__VERIFIER_assert N6:
(assert false)
(assert (let ((.def_168 (= |__VERIFIER_assert::cond| (_ bv0 32)))).def_168))

gcd_test N18:
(assert (let ((.def_197 (= |gcd_test::a| |main1::x|))).def_197))
(assert (let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_200 ((_ extract 31 31) .def_199)))(let ((.def_201 (= .def_200 (_ bv1 1)))).def_201))))
(assert (let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_380 ((_ extract 31 31) .def_379)))(let ((.def_381 (= .def_380 (_ bv1 1)))).def_381))))
(assert (let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_383 (bvsrem .def_199 .def_379)))(let ((.def_384 ((_ extract 7 0) .def_383)))(let ((.def_386 (= .def_384 |gcd_test::b|))).def_386))))))
(assert (let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_383 (bvsrem .def_199 .def_379)))(let ((.def_388 ((_ extract 31 31) .def_383)))(let ((.def_389 (= .def_388 (_ bv1 1)))).def_389))))))
(assert (let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_383 (bvsrem .def_199 .def_379)))(let ((.def_391 (bvslt .def_383 .def_379))).def_391)))))
(assert (let ((.def_393 (= |gcd_test::a| (_ bv0 8)))).def_393))
(assert (let ((.def_1011 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_1012 (bvsrem .def_379 .def_1011)))(let ((.def_1013 ((_ extract 7 0) .def_1012)))(let ((.def_1014 ((_ sign_extend 24) .def_1013)))(let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_1015 (bvslt .def_199 .def_1014))).def_1015))))))))
(assert (let ((.def_1011 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_1012 (bvsrem .def_379 .def_1011)))(let ((.def_1017 (bvslt .def_1012 .def_1011))).def_1017)))))
(assert (let ((.def_1011 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_1012 (bvsrem .def_379 .def_1011)))(let ((.def_1019 ((_ extract 31 31) .def_1012)))(let ((.def_1020 (= .def_1019 (_ bv1 1)))).def_1020))))))
(assert (let ((.def_1011 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_1012 (bvsrem .def_379 .def_1011)))(let ((.def_1022 (bvslt .def_379 .def_1012))).def_1022)))))
(assert (let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_1024 (bvslt (_ bv0 32) .def_199))).def_1024)))
(assert (let ((.def_1026 (= |gcd_test::b| (_ bv0 8)))).def_1026))
(assert (let ((.def_1011 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_1028 ((_ extract 31 31) .def_1011)))(let ((.def_1029 (= .def_1028 (_ bv1 1)))).def_1029))))
(assert (let ((.def_1011 ((_ sign_extend 24) |gcd_test::b|)))(let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_1031 (bvslt .def_199 .def_1011))).def_1031))))
(assert (let ((.def_379 ((_ sign_extend 24) |gcd_test::a|)))(let ((.def_199 ((_ sign_extend 24) |main1::x|)))(let ((.def_1033 (bvslt .def_199 .def_379))).def_1033))))

