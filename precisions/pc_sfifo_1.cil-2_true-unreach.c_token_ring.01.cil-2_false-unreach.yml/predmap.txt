(set-info :source |printed by MathSAT|)
(declare-fun c_num_read () (_ BitVec 32))
(declare-fun p_num_write () (_ BitVec 32))
(declare-fun c_last_read () (_ BitVec 32))
(declare-fun p_last_write () (_ BitVec 32))
(declare-fun c_dr_pc () (_ BitVec 32))
(declare-fun q_free () (_ BitVec 32))
(declare-fun q_buf_0 () (_ BitVec 32))
(declare-fun p_dw_pc () (_ BitVec 32))
(declare-fun q_read_ev () (_ BitVec 32))
(declare-fun c_dr_st () (_ BitVec 32))
(declare-fun p_dw_st () (_ BitVec 32))
(declare-fun m_pc () (_ BitVec 32))
(declare-fun t1_pc () (_ BitVec 32))
(declare-fun m_st () (_ BitVec 32))

*:
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))

error1 N4:
(assert false)
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))

do_write_p N64:
(assert (let ((.def_854 (= c_dr_pc (_ bv0 32)))).def_854))
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))
(assert (let ((.def_957 (= q_free (_ bv1 32)))).def_957))
(assert false)
(assert (let ((.def_1300 (= p_last_write q_buf_0))).def_1300))
(assert (let ((.def_960 (= p_dw_pc (_ bv1 32)))).def_960))
(assert (let ((.def_1487 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1488 (= p_num_write .def_1487))).def_1488)))
(assert (let ((.def_2381 (= q_free (_ bv0 32)))).def_2381))
(assert (let ((.def_1490 (= c_dr_pc (_ bv1 32)))).def_1490))
(assert (let ((.def_5183 (= q_read_ev (_ bv1 32)))).def_5183))

do_read_c N100:
(assert (let ((.def_960 (= p_dw_pc (_ bv1 32)))).def_960))
(assert (let ((.def_957 (= q_free (_ bv1 32)))).def_957))
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))
(assert (let ((.def_1300 (= p_last_write q_buf_0))).def_1300))
(assert (let ((.def_1487 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1488 (= p_num_write .def_1487))).def_1488)))
(assert (let ((.def_1490 (= c_dr_pc (_ bv1 32)))).def_1490))
(assert false)

eval1 N170:
(assert (let ((.def_854 (= c_dr_pc (_ bv0 32)))).def_854))
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))
(assert (let ((.def_957 (= q_free (_ bv1 32)))).def_957))
(assert (let ((.def_1230 (= p_dw_pc (_ bv0 32)))).def_1230))
(assert (let ((.def_1490 (= c_dr_pc (_ bv1 32)))).def_1490))
(assert (let ((.def_3804 (= c_dr_st (_ bv0 32)))).def_3804))
(assert (let ((.def_960 (= p_dw_pc (_ bv1 32)))).def_960))
(assert (let ((.def_5183 (= q_read_ev (_ bv1 32)))).def_5183))
(assert (let ((.def_5186 (= p_dw_st (_ bv0 32)))).def_5186))
(assert (let ((.def_1300 (= p_last_write q_buf_0))).def_1300))
(assert (let ((.def_1487 (bvadd (_ bv1 32) c_num_read)))(let ((.def_1488 (= p_num_write .def_1487))).def_1488)))

start_simulation1 N221:
(assert (let ((.def_854 (= c_dr_pc (_ bv0 32)))).def_854))
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))
(assert (let ((.def_957 (= q_free (_ bv1 32)))).def_957))
(assert (let ((.def_1230 (= p_dw_pc (_ bv0 32)))).def_1230))
(assert (let ((.def_5183 (= q_read_ev (_ bv1 32)))).def_5183))

error2 N260:
(assert false)
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))

eval2 N407:
(assert (let ((.def_6921 (= m_pc (_ bv0 32)))).def_6921))
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))
(assert (let ((.def_7517 (= t1_pc (_ bv0 32)))).def_7517))
(assert (let ((.def_7519 (= m_pc (_ bv1 32)))).def_7519))
(assert (let ((.def_7521 (= t1_pc (_ bv1 32)))).def_7521))
(assert (let ((.def_7524 (= m_st (_ bv0 32)))).def_7524))

start_simulation2 N557:
(assert (let ((.def_6921 (= m_pc (_ bv0 32)))).def_6921))
(assert (let ((.def_510 (= c_num_read p_num_write))).def_510))
(assert (let ((.def_519 (= c_last_read p_last_write))).def_519))
(assert (let ((.def_7517 (= t1_pc (_ bv0 32)))).def_7517))
(assert (let ((.def_7519 (= m_pc (_ bv1 32)))).def_7519))
(assert (let ((.def_7521 (= t1_pc (_ bv1 32)))).def_7521))

