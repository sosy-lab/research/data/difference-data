#!/usr/bin/env python3

# This file is part of the replication artifact for
# difference verification with conditions:
# https://gitlab.com/sosy-lab/research/data/difference-data
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
from pathlib import Path
import yaml
import zipfile

class ProcessingError(Exception):
    pass

def _unzip(zip_file, target):
    with zipfile.ZipFile(str(zip_file), 'r') as zip_ref:
        zip_ref.extractall(str(target))


def _get_yaml(yml_file):
    with open(yml_file) as yml_ref:
        return yaml.safe_load(yml_ref)


def _get_verdict(yml_content):
    for p in yml_content['properties']:
        if 'unreach-call.prp' in p['property_file']:
            return p['expected_verdict']
    else:
        raise ProcessingError("Task without unreach-call property: " + yml_content['input_file'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--benchmark-dir", dest="benchmark_dir", required=True)
    parser.add_argument("--property", default="../../../sv-benchmarks/c/properties/unreach-call.prp")
    parser.add_argument("result_files", nargs="+")

    args = parser.parse_args()

    args.benchmark_dir = Path(args.benchmark_dir).absolute()

    for d in args.result_files:
        base_path = Path(d).absolute()
        prop = args.property

        for z in base_path.rglob('**/*.zip'):
            zip_dir = z.parent
            _unzip(zip_file=z, target=zip_dir)

        for i in base_path.iterdir():
            print("Looking at", i.name)
            try:
                next(i.glob('*.yml'))
                print("Skipping", i.name, ": YML exists")
                continue
            except StopIteration:
                pass

            try:
                program = next(i.rglob('**/*.c'))
            except StopIteration:
                print("Skipping", i.name, ": No program exists")
                continue

            original_name = '.'.join(i.name.split('.')[1:])
            try:
                original_yml = next(args.benchmark_dir.rglob('**/' + original_name))
            except StopIteration:
                print("Skipping", i.name, ": No fitting original task")
                continue

            target_yml = i / Path(original_yml.name)
            print("Creating", target_yml, "for", i)
            original_yml_content = _get_yaml(original_yml)
            expected_verdict = _get_verdict(original_yml_content)
            new_yml_content = original_yml_content
            new_yml_content['input_files'] = str(program.relative_to(target_yml.parent))
            new_yml_content['properties'] = [{'property_file': str(prop), 'expected_verdict': expected_verdict}]

            with open(target_yml, 'w') as outp:
                yaml.dump(new_yml_content, stream=outp)
