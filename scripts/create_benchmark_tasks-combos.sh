#!/bin/bash

# This file is part of the replication artifact for
# difference verification with conditions:
# https://gitlab.com/sosy-lab/research/data/difference-data
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -eao pipefail
IFS=$'\t\n'

task_directory=../svcomp_combination_tasks

pushd "$task_directory" > /dev/null
i=0;
for base_prog in $@; do
    base_prog=$(basename $base_prog)
    base_prog_yml=${base_prog%%.c}
    base_prog_yml=${base_prog_yml%%.i}.yml
    prefix=${base_prog%%.c_*}
    prefix=${prefix%%.i_*}
    suffix=${base_prog#*.c_}
    suffix=${suffix#*.i_}
    suffix=${suffix%.c}
    suffix=${suffix%.i}
    taskname="base--$prefix--$suffix"
    echo "<tasks name=\"${taskname}\">
  <requiredfiles>\${taskdef_path}/${base_prog}</requiredfiles>
  <requiredfiles>../precisions/${base_prog_yml}/predmap.txt</requiredfiles>
  <option name=\"-setprop\">differential.program=\${taskdef_path}/${base_prog}</option>
  <option name=\"-setprop\">cpa.predicate.predmap.file=../precisions/${base_prog_yml}/predmap.txt</option>"

    if [[ $prefix == *true-unreach* ]]; then
        for inc_prog in $(echo "$@" | tr ' ' '\n' | grep "^$prefix.[ci]"); do
          [[ "$base_prog" == "$inc_prog" ]] && continue
          [[ "$suffix" == *false-unreach* ]] && [[ "$inc_prog" == *false-unreach* ]] && continue
          inc_name=${inc_prog%.c}
          inc_name=${inc_name%.i}
          task_def=${inc_name}.yml
          inc_task_def=${inc_name}-$i.yml
          [[ -e "$inc_task_def" ]] || cp "$task_def" "$inc_task_def"
          echo -e "\t<include>${task_directory}/${inc_task_def}</include>"
        done
    fi
    if [[ $suffix == *true-unreach* ]]; then
        for inc_prog in $(echo "$@" | tr ' ' '\n' | grep "$suffix.[ci]$"); do
          [[ "$base_prog" == "$inc_prog" ]] && continue
          [[ "$prefix" == *false-unreach* ]] && [[ "$inc_prog" == *false-unreach* ]] && continue
          inc_name=${inc_prog%.c}
          inc_name=${inc_name%.i}
          task_def=${inc_name}.yml
          inc_task_def=${inc_name}-$i.yml
          [[ -e "$inc_task_def" ]] || cp "$task_def" "$inc_task_def"
          echo -e "\t<include>${task_directory}/${inc_task_def}</include>"
        done
    fi

    echo "</tasks>"
    i=$((i+=1))
done
popd > /dev/null

echo '</benchmark>'
