#!/bin/bash

# This file is part of the replication artifact for
# difference verification with conditions:
# https://gitlab.com/sosy-lab/research/data/difference-data
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -eao pipefail
IFS=$'\t\n'

task_directory=${1:-./}

for i in ${task_directory}/*.ko; do
    taskname=$(echo "$i" | rev | cut -d"/" -f 1 | rev | cut -c10- | cut -d"." -f1)
    pushd "$i" > /dev/null
    for prog in $(ls | cut -d"." -f 3 | sort | uniq); do
        tasks=($(ls *$prog*))
        num_tasks=${#tasks[@]}
        ti=0
        while [[ $ti -lt $(($num_tasks - 1)) ]]; do
            next=$((ti + 1))
            base_task="${tasks[$ti]}"
            inc_task="${tasks[$next]}"
            inc_num=$(echo "$inc_task" | cut -d"." -f1)
            ti=$next

            if [[ "$base_task" == *"unsafe"* ]]; then
                continue
            fi

            echo -e "<tasks name=\"${taskname}-${prog}-${inc_num}\">\n  <requiredfiles>\${inputfile_path}/${base_task}</requiredfiles>\n  <option name=\"-setprop\">differential.program=\${inputfile_path}/${base_task}</option>\n  <include>${i}/${inc_task}</include>"
            echo -en "  <option name=\"-entryfunction\">"
            if grep -q ldv_main1_sequence_infinite_withcheck_stateful $inc_task; then
                echo -n "ldv_main1_sequence_infinite_withcheck_stateful"
            else
                echo -n "ldv_main0_sequence_infinite_withcheck_stateful"
            fi
            echo "</option>"
            echo "</tasks>"
        done
    done
    popd > /dev/null
done
