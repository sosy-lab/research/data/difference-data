// This file is part of the replication artifact for
// difference verification with conditions:
// https://gitlab.com/sosy-lab/research/data/difference-data
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org\>
//
// SPDX-License-Identifier: Apache-2.0

extern void __VERIFIER_error();
extern int __VERIFIER_nondet_int();

void main()
{
  int a=__VERIFIER_nondet_int();
  int r=0;
  if(a<0)
    while(a<0)
    {
      r=r-a;
      a=a+1;
    }
  else
  {
    r=a+a+1;
    r=r/2;
  }
  if(r<0)
    __VERIFIER_error();
}
